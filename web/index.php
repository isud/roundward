<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
			
			function convertDateBD($date){
				//$dateTimeExplode = explode(" ", $dateTime);
				//$date = $dateTimeExplode[0];
				//$time = $dateTimeExplode[1];

				$dateExplode = explode('-', $date);
				//$timeExplode = explode(':', $time);

				//convert year
				$bdYear = $dateExplode[0] + 543;
				//convert month
				switch ($dateExplode[1]) {
					case 01:
						$bdMonth = 'มกราคม';
						break;
					case 02:
						$bdMonth = 'กุมภาพันธ์';
						break;
					case 03:
						$bdMonth = 'มีนาคม';
						break;
					case 04:
						$bdMonth = 'เมษายน';
						break;
					case 05:
						$bdMonth = 'พฤษภาคม';
						break;
					case 06:
						$bdMonth = 'มิถุนายน';
						break;
					case 07:
						$bdMonth = 'กรกฎาคม';
						break;
					case 08:
						$bdMonth = 'สิงหาคม';
						break;
					case 09:
						$bdMonth = 'กันยายน';
						break;
					case 10:
						$bdMonth = 'ตุลาคม';
						break;
					case 11:
						$bdMonth = 'พฤศจิกายน';
						break;
					case 12:
						$bdMonth = 'ธันวาคม';
						break;
				}
				$bdDate = $dateExplode[2];
				//$time = $time;
				
				return $bdDate." ".$bdMonth." ".$bdYear;
			}
		
			function getRoundShift(){
				$sql = "SELECT * FROM mas_round_shift WHERE status = '1';";
				$result = mysql_query($sql);
				
				while($row = mysql_fetch_array($result)){
					$round_shift_id = $row["round_shift_id"];
					$round_shift_name = $row["round_shift_name"];
					
					echo "<option value=$round_shift_id>".$round_shift_name."</option>";
				}
			}
			
			function getClinic(){
				$sql = "SELECT * FROM mas_round_clinic WHERE status = '1';";
				$result = mysql_query($sql);
				
				while($row = mysql_fetch_array($result)){
					$clinic_id = $row["clinic_id"];
					$clinic_name = $row["clinic_name"];
					
					echo "<option value=$clinic_id>".$clinic_name."</option>";
				}
			}
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Nurse Round Ward</title>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		
		<!-- slider CSS -->
		<link href="css/ion.rangeSlider.css" rel="stylesheet" type="text/css" /> 
		<link href="css/normalize.css" rel="stylesheet" type="text/css" /> 
		<link href="css/ion.rangeSlider.skinHTML5.css" rel="stylesheet" type="text/css" /> 
		<link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico' />
		
		<!-- switch -->
		<link href="css/bootstrap-switch.min.css" rel="stylesheet">
		
		<style>
			.caption{
				font-size: 14pt;
			}
			
			.form-group{
				margin-bottom: 25px;
			}
			
			.col-sm-9{
				margin-bottom: 25px;
			}
		</style>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-sm-4 col-sm-offset-4'>
					<form method='post' action=''>
						<center>
							<h3>ยินดีต้อนรับ</h3>
							<h4>โปรดกรุณากรอกชื่อผู้ใช้งานและรหัสผ่านเพื่อเข้าสู่ระบบ</h4>
						</center>
						<div class="form-group">
							<label for="user">ชื่อผู้ใช้งาน</label>
							<input type="text" class="form-control" id="user" placeholder="Username" name='username'>
						</div>
						<div class="form-group">
							<label for="password">รหัสผ่าน</label>
							<input type="password" class="form-control" id="password" placeholder="Password" name='password'>
						</div>
						<button type="submit" class="btn btn-block btn-default" name='logIn'>เข้าสู่ระบบ</button>
					</form>
				</div>
			</div>
		</div>
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ion.rangeSlider.min.js"></script>
	<script src="js/bootstrap-switch.min.js"></script>
	<script src="jsControl/sliderConf.js"></script>
	
	<!-- switch section -->
	<script src="jsControl/switchConf.js"></script>
	
	</body>
</html>

<?php
	if(isset($_POST['logIn'])){		
		try {
			$sql = "SELECT * FROM mas_user WHERE user_name = '$_POST[username]' AND password = '$_POST[password]';";
			$result = mysql_query($sql);
			$num_rows = mysql_num_rows($result);
			
			if($num_rows == 0){
				echo "<script language='javascript'>";
				echo "alert('ท่านระบุ username หรือ password ไม่ถูกต้อง กรุณาระบุใหม่อีกครั้ง')";
				echo  "</script>";
			}else{
				while($fetcharr=mysql_fetch_array($result)){
					$userid = $fetcharr['user_id'];
					$user_name = $fetcharr['user_name'];
					$password = $fetcharr['password'];
					$name = $fetcharr['name'].' '.$fetcharr['lname'];
					$status = $fetcharr['status'];
				}
				if($status == 1){
					$_SESSION['user_id'] = $userid;
					//$_SESSION['USER_ACCOUNT'] = $USER_ACCOUNT;
					$_SESSION['name'] = $name;
					$_SESSION['status'] = $status;
					echo "<script language=\"javascript\">window.location='".basename("pointing.php")."'</script>";
				}else{
					echo "<script language='javascript'>";
					echo "alert('บัญชีใช้งานของท่านถูกระงับการใช้งาน โปรดติดต่อผู้ดูแลระบบ')";
					echo  "</script>";
					echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
				}
			}
			
			//echo "<script language='javascript'>";
			//echo "alert('ให้คะแนนเรียบร้อยแล้ว')";
			//echo  "</script>";
			
			//echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
?>