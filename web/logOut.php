<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<META HTTP-EQUIV="Refresh" CONTENT="2;URL=index.php">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Nurse Round Ward</title>
		
		<link href="css/bootstrap.min.css" rel="stylesheet">
		
		<!-- slider CSS -->
		<link href="css/ion.rangeSlider.css" rel="stylesheet" type="text/css" /> 
		<link href="css/normalize.css" rel="stylesheet" type="text/css" /> 
		<link href="css/ion.rangeSlider.skinHTML5.css" rel="stylesheet" type="text/css" /> 
		
		<!-- switch -->
		<link href="css/bootstrap-switch.min.css" rel="stylesheet">
		
		<style>
			.caption{
				font-size: 14pt;
			}
			
			.form-group{
				margin-bottom: 25px;
			}
			
			.col-sm-9{
				margin-bottom: 25px;
			}
		</style>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-sm-4 col-sm-offset-4'>
					<center>
						<h2>logging out...<br>กำลังออกจากระบบ...</h2>
					</center>
					<?php
						session_destroy();
					?>
				</div>
			</div>
		</div>
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ion.rangeSlider.min.js"></script>
	<script src="js/bootstrap-switch.min.js"></script>
	<script src="jsControl/sliderConf.js"></script>
	
	<!-- switch section -->
	<script src="jsControl/switchConf.js"></script>
	
	</body>
</html>


