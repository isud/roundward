<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
			
			function convertDateBD($date){
				//$dateTimeExplode = explode(" ", $dateTime);
				//$date = $dateTimeExplode[0];
				//$time = $dateTimeExplode[1];

				$dateExplode = explode('-', $date);
				//$timeExplode = explode(':', $time);

				//convert year
				$bdYear = $dateExplode[0] + 543;
				//convert month
				switch ($dateExplode[1]) {
					case 01:
						$bdMonth = 'มกราคม';
						break;
					case 02:
						$bdMonth = 'กุมภาพันธ์';
						break;
					case 03:
						$bdMonth = 'มีนาคม';
						break;
					case 04:
						$bdMonth = 'เมษายน';
						break;
					case 05:
						$bdMonth = 'พฤษภาคม';
						break;
					case 06:
						$bdMonth = 'มิถุนายน';
						break;
					case 07:
						$bdMonth = 'กรกฎาคม';
						break;
					case 08:
						$bdMonth = 'สิงหาคม';
						break;
					case 09:
						$bdMonth = 'กันยายน';
						break;
					case 10:
						$bdMonth = 'ตุลาคม';
						break;
					case 11:
						$bdMonth = 'พฤศจิกายน';
						break;
					case 12:
						$bdMonth = 'ธันวาคม';
						break;
				}
				$bdDate = $dateExplode[2];
				//$time = $time;
				
				return $bdDate." ".$bdMonth." ".$bdYear;
			}
		
			function getRoundShift(){
				$sql = "SELECT * FROM mas_round_shift WHERE status = '1';";
				$result = mysql_query($sql);
				
				while($row = mysql_fetch_array($result)){
					$round_shift_id = $row["round_shift_id"];
					$round_shift_name = $row["round_shift_name"];
					
					if($_SESSION['roundShift'] == $round_shift_id){
						echo "<option value=$round_shift_id selected>".$round_shift_name."</option>";
					}else{
						echo "<option value=$round_shift_id>".$round_shift_name."</option>";
					}
					
				}
			}
			
			function getClinic(){
				$sql = "SELECT floor_id, floor_name FROM mas_round_floor WHERE `status` = 1;";
				
				$result = mysql_query($sql);
				
				while($row = mysql_fetch_array($result)){
					$floor_id = $row["floor_id"];
					$floor_name = $row["floor_name"];
					
					echo '<optgroup label="'.$floor_name.'">';
					
					$sql2 = "SELECT * FROM mas_round_clinic WHERE status = '1' AND floor_id = '$floor_id';";
					$result2 = mysql_query($sql2);
					while($row2 = mysql_fetch_array($result2)){
						$clinic_id = $row2["clinic_id"];
						$clinic_name = $row2["clinic_name"];
					
						echo "<option value=$clinic_id>".$clinic_name."</option>";
					}
					
					echo "</optgroup>";
				}
			}
			
			function uploadPic($tmpFile, $picName, $numberPic){
				//random File Name
				$randomBannerName = mt_rand(1, 99);
				
				//file type
				$subFileType = explode(".", $picName);
				$fileTypeFromName = $subFileType[1];
				
				$newPicName = date('Y-m-d-H-i-s').'num'.$numberPic.'ran'.$randomBannerName.'rw.'.$fileTypeFromName;
				if(move_uploaded_file($tmpFile, "roundPic/".$newPicName)){
					return $newPicName;
				}
			}
			
			if($_SESSION['user_id'] != ""){
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Nurse Round Ward</title>
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico' />
		
		<!-- slider CSS -->
		<link href="css/ion.rangeSlider.css" rel="stylesheet" type="text/css" /> 
		<link href="css/normalize.css" rel="stylesheet" type="text/css" /> 
		<link href="css/ion.rangeSlider.skinHTML5.css" rel="stylesheet" type="text/css" /> 
		
		<!-- switch -->
		<link href="css/bootstrap-switch.min.css" rel="stylesheet">
		
		<style>
			.caption{
				font-size: 14pt;
			}
			
			.form-group{
				margin-bottom: 25px;
			}
			
			.col-sm-9{
				margin-bottom: 25px;
			}
			
			nav{
				margin-top: -21px;
			}
		</style>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ระบบประเมินการดำเนินการ</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="#">ตรวจการ</a></li>
						<li><a href="pointingList.php">ดูข้อมูลย้อนหลัง</a></li>
					</ul>
			  
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#"><?php echo $_SESSION['name'];?></a></li>
						<li><a href="logOut.php"><strong>log out</strong></a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		
		<div class='container-fluid'>
			<div class='col-sm-11'>
				<!--
				<input type="text" id="example_id" class='slider' name="example_name" value="" />
				-->
				
				<form class="form-horizontal" method='post' action='' enctype="multipart/form-data">
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-3 control-label caption">แผนก</label>
						<div class="col-sm-9 form-group">
							<select class='form-control' name='clinic'>
								<?php
									getClinic();
								?>
							</select>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">เวร</label>
						<div class="col-sm-9 form-group">
							<select class='form-control' name='roundShift'>
								<?php
									getRoundShift();
								?>
							</select>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">ปกติ</label>
						<div class="col-sm-9 form-group">
							<input type="checkbox" class='toggleSwitch' value='1' name="normal">
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">ลาป่วย</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="sickLeave"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">ลากิจ</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="errandLeave"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">Work Load</label>
						<div class="col-sm-9 form-group">
							<input type="checkbox" class='toggleSwitch' value='1' name="workLoad">
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">อุบัติเหตุพนักงาน</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="empAccident"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">เครื่องมือเวชภัณฑ์ ไม่พอ</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="insufMedical"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">เอกสารผู้ป่วย</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="patDocument"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">ระบบงานผิดพลาด</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="falseProcess"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">สิ่งแวดล้อม แสง/สี/เสียง</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="environment"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">ลูกค้าร้องเรียน</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="complain"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">อุบัติเหตุผู้ป่วย</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="patAccident"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">ไม่มีญาติ</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="noKin"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">พิจารณาปัญหาทางการเงิน</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="financeProblem"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">ของสูญหาย</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="missMaterial"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">รถพยาบาล</label>
						<div class="col-sm-9">
							<input type="text" class='slider' value=""  name="ambulance"/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption" style='margin-top:10px;'>อื่นๆ</label>
						<div class="col-sm-9"  style='margin-top:15px;'>
							<textarea name='other' class='form-control' rows="8"></textarea>
						</div>
						
						<!-- attach Pic -->
						<label for="inputEmail3" class="col-sm-3 control-label caption">แนบรูป</label>
						<div class="col-sm-9">
							<input type="file" name='attachPic'/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">แนบรูป2</label>
						<div class="col-sm-9">
							<input type="file" name='attachPic2'/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">แนบรูป3</label>
						<div class="col-sm-9">
							<input type="file" name='attachPic3'/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">แนบรูป4</label>
						<div class="col-sm-9">
							<input type="file" name='attachPic4'/>
						</div>
						
						<label for="inputEmail3" class="col-sm-3 control-label caption">แนบรูป5</label>
						<div class="col-sm-9">
							<input type="file" name='attachPic5'/>
						</div>
						
						<!-- end of attach Pic -->
						
						<div class="col-sm-offset-3 col-sm-9">
							<br>
							<button class='submit btn btn-primary btn-lg btn-block' name='submitScore' type='submit'>ตกลง</button>
						</div>
					</div>
				</form>
				
			</div>
		</div>
		
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ion.rangeSlider.min.js"></script>
	<script src="js/bootstrap-switch.min.js"></script>
	<script src="jsControl/sliderConf.js"></script>
	
	<!-- switch section -->
	<script src="jsControl/switchConf.js"></script>
	
	</body>
</html>

<?php
	}else{
		echo "<script language='javascript'>";
		echo "alert('ท่านยังไม่ได้ login เข้าสู่ระบบ โปรดกรุณา login ก่อน')";
		echo  "</script>";
		
		echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
	}

	if(isset($_POST['submitScore'])){		
		try {
			$sql1 = "";
			$sql2 = "";
			
			$sql1 .= "normal, ";
			if(isset($_POST['normal'])){
				$sql2 .= "0, ";
			}else{
				$sql2 .= "1, ";
			}
			
			$sql1 .= "sickLeave, ";
			$sql2 .= "'$_POST[sickLeave]', ";
			
			$sql1 .= "errandLeave, ";
			$sql2 .= "'$_POST[errandLeave]', ";
			
			$sql1 .= "workLoad, ";
			if(isset($_POST['workLoad'])){
				$sql2 .= "'$_POST[workLoad]', ";
			}else{
				$sql2 .= "0, ";
			}
			
			$sql1 .= "empAccident, ";
			$sql2 .= "'$_POST[empAccident]', ";
			
			$sql1 .= "insufMedical, ";
			$sql2 .= "'$_POST[insufMedical]', ";
			
			$sql1 .= "patDocument, ";
			$sql2 .= "'$_POST[patDocument]', ";
			
			$sql1 .= "falseProcess, ";
			$sql2 .= "'$_POST[falseProcess]', ";
			
			$sql1 .= "environment, ";
			$sql2 .= "'$_POST[environment]', ";
			
			$sql1 .= "complain, ";
			$sql2 .= "'$_POST[complain]', ";
			
			$sql1 .= "patAccident, ";
			$sql2 .= "'$_POST[patAccident]', ";
			
			$sql1 .= "noKin, ";
			$sql2 .= "'$_POST[noKin]', ";
			
			$sql1 .= "financeProblem, ";
			$sql2 .= "'$_POST[financeProblem]', ";
			
			$sql1 .= "missMaterial, ";
			$sql2 .= "'$_POST[missMaterial]', ";
			
			$sql1 .= "ambulance, ";
			$sql2 .= "'$_POST[ambulance]', ";
			
			$sql1 .= "other, ";
			$other = mysql_real_escape_string($_POST['other']);
			$sql2 .= "'$other', ";
			
			$sql1 .= "clinic, ";
			$sql2 .= "'$_POST[clinic]', ";
			
			$sql1 .= "date_round, ";
			$sql2 .= "NOW(), ";
			
			//attach pic
			
			if(!empty($_FILES["attachPic"]["name"])){
				$picName = uploadPic($_FILES["attachPic"]["tmp_name"], $_FILES["attachPic"]["name"], 1);
				$sql1 .= "img_path, ";
				$sql2 .= "'$picName', ";
			}
			
			if(!empty($_FILES["attachPic2"]["name"])){
				$picName2 = uploadPic($_FILES["attachPic2"]["tmp_name"], $_FILES["attachPic2"]["name"], 2);
				$sql1 .= "img_path2, ";
				$sql2 .= "'$picName2', ";
			}
			
			if(!empty($_FILES["attachPic3"]["name"])){
				$picName3 = uploadPic($_FILES["attachPic3"]["tmp_name"], $_FILES["attachPic3"]["name"], 3);
				$sql1 .= "img_path3, ";
				$sql2 .= "'$picName3', ";
			}
			
			if(!empty($_FILES["attachPic4"]["name"])){
				$picName4 = uploadPic($_FILES["attachPic4"]["tmp_name"], $_FILES["attachPic4"]["name"], 4);
				$sql1 .= "img_path4, ";
				$sql2 .= "'$picName4', ";
			}
			
			if(!empty($_FILES["attachPic5"]["name"])){
				$picName5 = uploadPic($_FILES["attachPic5"]["tmp_name"], $_FILES["attachPic5"]["name"], 5);
				$sql1 .= "img_path5, ";
				$sql2 .= "'$picName5', ";
			}
			
			//end of attach pic
			
			$sql1 .= "user_id, ";
			$sql2 .= "'$_SESSION[user_id]', ";
			
			$sql1 .= "pointType_id, ";
			$sql2 .= "'1', ";
			
			$sql1 .= "round_shift";
			$sql2 .= "'$_POST[roundShift]'";
			$_SESSION['roundShift'] = $_POST['roundShift'];
			
			$sql3 = "INSERT INTO round_point (".$sql1.") VALUES (".$sql2.");";
			
			mysql_query($sql3);
			
			//echo $sql3;
			
			echo "<script language='javascript'>";
			echo "alert('ทำการประเมินเรียบร้อยแล้ว')";
			echo  "</script>";
			
			echo "<script language=\"javascript\">window.location='".basename($_SERVER['PHP_SELF'])."'</script>";
		} catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
	}
?>