<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<?php
			include "include/connectdb.php";
			
			function convertDateBD($date){
				//$dateTimeExplode = explode(" ", $dateTime);
				//$date = $dateTimeExplode[0];
				//$time = $dateTimeExplode[1];

				$dateExplode = explode('-', $date);
				//$timeExplode = explode(':', $time);

				//convert year
				$bdYear = $dateExplode[0] + 543;
				//convert month
				switch ($dateExplode[1]) {
					case 01:
						$bdMonth = 'มกราคม';
						break;
					case 02:
						$bdMonth = 'กุมภาพันธ์';
						break;
					case 03:
						$bdMonth = 'มีนาคม';
						break;
					case 04:
						$bdMonth = 'เมษายน';
						break;
					case 05:
						$bdMonth = 'พฤษภาคม';
						break;
					case 06:
						$bdMonth = 'มิถุนายน';
						break;
					case 07:
						$bdMonth = 'กรกฎาคม';
						break;
					case 08:
						$bdMonth = 'สิงหาคม';
						break;
					case 09:
						$bdMonth = 'กันยายน';
						break;
					case 10:
						$bdMonth = 'ตุลาคม';
						break;
					case 11:
						$bdMonth = 'พฤศจิกายน';
						break;
					case 12:
						$bdMonth = 'ธันวาคม';
						break;
				}
				$bdDate = $dateExplode[2];
				//$time = $time;
				
				return $bdDate." ".$bdMonth." ".$bdYear;
			}
		
			function getRoundShift(){
				$sql = "SELECT * FROM mas_round_shift WHERE status = '1';";
				$result = mysql_query($sql);
				
				while($row = mysql_fetch_array($result)){
					$round_shift_id = $row["round_shift_id"];
					$round_shift_name = $row["round_shift_name"];
					
					echo "<option value=$round_shift_id>".$round_shift_name."</option>";
				}
			}
			
			function getClinic(){
				$sql = "SELECT floor_id, floor_name FROM mas_round_floor WHERE `status` = 1;";
				
				$result = mysql_query($sql);
				
				while($row = mysql_fetch_array($result)){
					$floor_id = $row["floor_id"];
					$floor_name = $row["floor_name"];
					
					echo '<optgroup label="'.$floor_name.'">';
					
					$sql2 = "SELECT * FROM mas_round_clinic WHERE status = '1' AND floor_id = '$floor_id';";
					$result2 = mysql_query($sql2);
					while($row2 = mysql_fetch_array($result2)){
						$clinic_id = $row2["clinic_id"];
						$clinic_name = $row2["clinic_name"];
					
						echo "<option value=$clinic_id>".$clinic_name."</option>";
					}
					
					echo "</optgroup>";
				}
			}
			
			function uploadPic($tmpFile, $picName){
				//random File Name
				$randomBannerName = mt_rand(1, 99);
				
				//file type
				$subFileType = explode(".", $picName);
				$fileTypeFromName = $subFileType[1];
				
				$newPicName = date('Y-m-d-H-i-s').'ran'.$randomBannerName.'rw.'.$fileTypeFromName;
				if(move_uploaded_file($tmpFile, "roundPic/".$newPicName)){
					return $newPicName;
				}
			}
			
			if($_SESSION['user_id'] != ""){
		?>
		<meta charset="utf-8">
		<!-- Clear Cache -->
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		<!--End of Clear Cache -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Nurse Round Ward</title>
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico' />
		
		<!-- slider CSS -->
		<link href="css/ion.rangeSlider.css" rel="stylesheet" type="text/css" /> 
		<link href="css/normalize.css" rel="stylesheet" type="text/css" /> 
		<link href="css/ion.rangeSlider.skinHTML5.css" rel="stylesheet" type="text/css" /> 
		
		<!-- switch -->
		<link href="css/bootstrap-switch.min.css" rel="stylesheet">
		
		<!-- datePicker -->
		<link href="css/bootstrap-datepicker3.standalone.min.css" rel="stylesheet">
		<link href="css/bootstrap-datepicker3.min.css" rel="stylesheet">
		
		<style>
			.caption{
				font-size: 14pt;
			}
			
			.form-group{
				margin-bottom: 25px;
			}
			
			.col-sm-9{
				margin-bottom: 25px;
			}
			
			nav{
				margin-top: -21px;
			}
		</style>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="js/html5shiv.min.js"></script>
		  <script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ระบบประเมินการดำเนินการ</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="pointing.php">ตรวจการ</a></li>
						<li><a href="#">ดูข้อมูลย้อนหลัง</a></li>
						
						<form class="navbar-form navbar-left" role="search">
							<input type="text" class="form-control input-sm" id='opDate'>
							<button type="button" id="optimizePoint" class="btn btn-sm btn-danger">ปรับคะแนน</button>
						</form>
					</ul>
			  
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#"><?php echo $_SESSION['name'];?></a></li>
						<li><a href="logOut.php"><strong>log out</strong></a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-sm-12'>
					<form class="form-horizontal" method='post' action='' enctype="multipart/form-data">
						<label for="inputEmail3" class="col-sm-2 control-label">เลือกช่วงวัน:</label>
						<div class="col-sm-10">
							<div class="col-sm-8 input-daterange input-group" id="datepicker">
								<input type="text" class="form-control" name="dpStartDate" />
								<span class="input-group-addon">ถึง</span>
								<input type="text" class="form-control" name="dpEndDate" />
								<span class="input-group-btn">
							        <button class="btn btn-default" type="button" id='btnSearchByDate'>ค้นหา</button>
							    </span>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class='row'>
				<div class='col-sm-12'>
					<br>
					<span class='allResult'>

					</span>

					<div class="modal fade bs-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog modal-lg" role="document">
						    <div class="modal-content">
						      	<div class="modal-header">
						        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        	<h4 class="modal-title" id="exampleModalLabel">ข้อมูลการตรวจการ</h4>
						      	</div>
						      	<div class="modal-body">
							        <form>
							          	<div class="form-group">
								            <label for="recipient-name" class="control-label">id:</label>
								            <span class='mdRound_point_id'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ผู้ประเมิน:</label>
							            	<span class='mdUserName'></span>
							          	</div>
										<div class="form-group">
							            	<label for="message-text" class="control-label">รูปแบบการประเมิน:</label>
							            	<span class='mdPointType'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">วันที่ประเมิน:</label>
							            	<span class='mdDate_round'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">แผนก:</label>
							            	<span class='mdClinicName'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">เวร:</label>
							            	<span class='mdShift'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">เหตุการณ์ปกติ:</label>
							            	<span class='mdNormal'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ลาป่วย:</label>
							            	<span class='mdSickLeave'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ลากิจ:</label>
							            	<span class='mdErrandLeave'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">งานโหลด:</label>
							            	<span class='mdWorkLoad'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">อุบัติเหตุพนักงาน:</label>
							            	<span class='mdEmpAccident'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ยา/เวชภัณฑ์ไม่เพียงพอ:</label>
							            	<span class='mdInsufMedical'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">เอกสารผู้ป่วย:</label>
							            	<span class='mdPatDocument'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ระบบงานผิดพลาด:</label>
							            	<span class='mdFalseProcess'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">สิ่งแวดล้อม:</label>
							            	<span class='mdEnvironment'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ลูกค้าร้องเรียน:</label>
							            	<span class='mdComplain'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">อุบัติเหตุผู้ป่วย:</label>
							            	<span class='mdPatAccident'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ไม่มีญาติ:</label>
							            	<span class='mdNoKin'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">ปัญหาด้านการเงิน:</label>
							            	<span class='mdFinanceProblem'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">อุปกรณ์สูญหาย:</label>
							            	<span class='mdMissMaterial'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">รถพยาบาล:</label>
							            	<span class='mdAmbulance'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label">อื่นๆ:</label>
							            	<span class='mdOther'></span>
							          	</div>
							          	<div class="form-group">
							            	<label for="message-text" class="control-label" id='lblImg'>รูปภาพแนบ:</label>
							            	<span class='mdImgPath'></span>
							          	</div>
										<div class="form-group">
							            	<label for="message-text" class="control-label" id='lblImg2'>รูปภาพแนบ2:</label>
							            	<span class='mdImgPath2'></span>
							          	</div>
										<div class="form-group">
							            	<label for="message-text" class="control-label" id='lblImg3'>รูปภาพแนบ3:</label>
							            	<span class='mdImgPath3'></span>
							          	</div>
										<div class="form-group">
							            	<label for="message-text" class="control-label" id='lblImg4'>รูปภาพแนบ4:</label>
							            	<span class='mdImgPath4'></span>
							          	</div>
										<div class="form-group">
							            	<label for="message-text" class="control-label" id='lblImg5'>รูปภาพแนบ5:</label>
							            	<span class='mdImgPath5'></span>
							          	</div>
							        </form>
						      	</div>
						      	<div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
						      	</div>
						 	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ion.rangeSlider.min.js"></script>
	<script src="js/bootstrap-switch.min.js"></script>
	<script src="jsControl/sliderConf.js"></script>
	
	<!-- switch section -->
	<script src="jsControl/switchConf.js"></script>
	
	<!-- datePicker -->
	<script src="js/bootstrap-datepicker.min.js"></script>
	
	<script>
		//datepicker
		$('.input-daterange').datepicker({
			format: "yyyy-mm-dd"
		});
		
		$('#opDate').datepicker({
			format: "yyyy-mm-dd"
		});

		//load data
		$("#btnSearchByDate").click(function(){
			$.post("ajax/pointByDate.php", { 
				date1: $("input[name=dpStartDate]").val(), 
				date2: $("input[name=dpEndDate]").val()}, 
				function(result){
					$(".allResult").html(result);
				}
			);
		});

		$(document).on('click', '.seeInfo' ,function() {
			var point_id = $(this).closest('tr').children('td.point_id').text();
			
			$('#exampleModal').modal('show');

			$.post("ajax/getPointDetail.php", { 
			point_id: point_id}, 
				function(result){
					$(".mdRound_point_id").html(point_id);
					$(".mdUserName").html(result.evalName);
					$(".mdPointType").html(result.pointType);
					$(".mdNormal").html(result.normal);
					$(".mdDate_round").html(result.date_round);
					$(".mdClinicName").html(result.clinic_name);
					$(".mdShift").html(result.round_shift_name);
					$(".mdSickLeave").html(result.sickLeave);
					$(".mdErrandLeave").html(result.errandLeave);
					$(".mdWorkLoad").html(result.workLoad);
					$(".mdEmpAccident").html(result.empAccident);
					$(".mdInsufMedical").html(result.insufMedical);
					$(".mdPatDocument").html(result.patDocument);
					$(".mdFalseProcess").html(result.falseProcess);
					$(".mdEnvironment").html(result.environment);
					$(".mdComplain").html(result.complain);
					$(".mdPatAccident").html(result.patAccident);
					$(".mdNoKin").html(result.noKin);
					$(".mdFinanceProblem").html(result.financeProblem);
					$(".mdMissMaterial").html(result.missMaterial);
					$(".mdAmbulance").html(result.ambulance);
					$(".mdOther").html(result.other);

					if(result.img_path !== null){
						$("#lblImg").show();
						$(".mdImgPath").html("<a href='roundPic/" + result.img_path + "' target='_blank'>" + "<img class='img-responsive' src='roundPic/" + result.img_path + "'>" + "</a>");
					}else{
						$("#lblImg").hide();
						$(".mdImgPath").html("");
					}
					
					if(result.img_path2 !== null){
						$("#lblImg2").show();
						$(".mdImgPath2").html("<a href='roundPic/" + result.img_path2 + "' target='_blank'>" + "<img class='img-responsive' src='roundPic/" + result.img_path2 + "'>" + "</a>");
					}else{
						$("#lblImg2").hide();
						$(".mdImgPath2").html("");
					}
					
					if(result.img_path3 !== null){
						$("#lblImg3").show();
						$(".mdImgPath3").html("<a href='roundPic/" + result.img_path3 + "' target='_blank'>" + "<img class='img-responsive' src='roundPic/" + result.img_path3 + "'>" + "</a>");
					}else{
						$("#lblImg3").hide();
						$(".mdImgPath3").html("");
					}
					
					if(result.img_path4 !== null){
						$("#lblImg4").show();
						$(".mdImgPath4").html("<a href='roundPic/" + result.img_path4 + "' target='_blank'>" + "<img class='img-responsive' src='roundPic/" + result.img_path4 + "'>" + "</a>");
					}else{
						$("#lblImg4").hide();
						$(".mdImgPath4").html("");
					}
					
					if(result.img_path5 !== null){
						$("#lblImg5").show();
						$(".mdImgPath5").html("<a href='roundPic/" + result.img_path5 + "' target='_blank'>" + "<img class='img-responsive' src='roundPic/" + result.img_path5 + "'>" + "</a>");
					}else{
						$("#lblImg5").hide();
						$(".mdImgPath5").html("");
					}
				}
			);
		});
	
	
		$("#optimizePoint").click(function(){
			var opDate = $("#opDate").val();
			
			if(opDate == ''){
				alert("โปรดกรุณาระบุวันที่ต้องการปรับข้อมูล");
			}else{
				if (confirm("ท่านแน่ใจหรือไม่ว่าต้องการปรับข้อมูล?")) {
				
					$.ajax({ 
						url: "ajax/optimizePoint.php", 
						method: "POST", 
						data: {date: $("#opDate").val(), user_id: <?php echo $_SESSION['user_id'];?>},
						beforeSend: function(){
							$("#optimizePoint").html("ปรับคะแนน...กำลังโหลดข้อมูล");
						}
					})
						
					.success(function(result){
						alert("ทำการประเมินอัตโนมัติเรียบร้อยแล้ว");
					})
					.error(function() { 
						alert("ระบบข้อมูลมีปัญหา โปรดแจ้งผู้ดูแลระบบ");
					})
					.complete(function() {
						$("#optimizePoint").html("ปรับคะแนน");
					});
				} else {
					
				}
			}
		});
	</script>
	
	</body>
</html>

<?php
	}else{
		echo "<script language='javascript'>";
		echo "alert('ท่านยังไม่ได้ login เข้าสู่ระบบ โปรดกรุณา login ก่อน')";
		echo  "</script>";
		
		echo "<script language=\"javascript\">window.location='".basename("index.php")."'</script>";
	}
?>