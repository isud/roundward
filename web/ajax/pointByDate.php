<?php
	include "../include/connectdb.php";

	$sql = "SELECT round_point_id, CASE normal WHEN 1 THEN 'ปกติ' ELSE 'ไม่ปกติ' END AS 'normal', clinic, mas_round_clinic.clinic_name, date_round, round_shift, mas_round_shift.round_shift_name, round_point.user_id, mas_user.evalName, point_type.`pointType` FROM (SELECT * FROM round_point WHERE date_round >= '$_POST[date1] 00:00:00' AND date_round <= '$_POST[date2] 23:59:59') AS round_point JOIN mas_round_clinic ON mas_round_clinic.`clinic_id` = round_point.clinic JOIN mas_round_shift ON mas_round_shift.round_shift_id = round_point.round_shift JOIN (SELECT user_id, CONCAT_WS(' ', NAME, lname) AS 'evalName' FROM mas_user) AS mas_user ON mas_user.`user_id` = round_point.`user_id` LEFT JOIN point_type ON point_type.`pointType_id` = round_point.`pointType_id` ORDER BY date_round ASC, clinic";
	$result = mysql_query($sql);
	$numRows = mysql_num_rows($result);

	if($numRows == 0){
		echo "ไม่พบผลการค้นหาใดๆ โปรดเลือกวันที่ใหม่";
	}else{
		echo "<table class='table table-striped table-responsive'>";

		echo "<tr>";
		echo "<th>".'id'."</th>";
		echo "<th>".'แผนก'."</th>";
		echo "<th>".'วันที่ตรวจ'."</th>";
		echo "<th>".'เวร'."</th>";
		echo "<th>".'ผู้ประเมิน'."</th>";
		echo "<th>".'รูปแบบการประเมิน'."</th>";
		echo "<th>".'สถานการณ์'."</th>";
		echo "<th>".'ดูข้อมูล'."</th>";
		echo "</tr>";

		while($row = mysql_fetch_array($result)){
			$round_point_id = $row["round_point_id"];
			$clinic_name = $row["clinic_name"];
			$date_round = $row["date_round"];
			$round_shift_name = $row["round_shift_name"];
			$evalName = $row["evalName"];
			$pointType = $row["pointType"];
			$normal = $row["normal"];
			
			if($normal == 'ปกติ'){
				echo "<tr>";
			}else{
				echo "<tr class='danger'>";
			}
			
			echo "<td class='point_id'>".$round_point_id."</td>";
			echo "<td>".$clinic_name."</td>";
			echo "<td>".$date_round."</td>";
			echo "<td>".$round_shift_name ."</td>";
			echo "<td>".$evalName."</td>";
			echo "<td>".$pointType."</td>";
			echo "<td>".$normal."</td>";
			echo "<td>"."<button class='btn btn-info seeInfo'>ดูข้อมูล</button>"."</td>";
			echo "</tr>";
		}

		echo "</table>";
	}
	mysql_close();
?>