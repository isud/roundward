$("[name='normal']").bootstrapSwitch({
	onText: "ไม่ปกติ",
	onColor: "danger",
	offText: "ปกติ",
	offColor: "success"
});

$("[name='abnormal']").bootstrapSwitch({
	onText: "ไม่ปกติ",
	onColor: "danger",
	offText: "ปกติ",
	offColor: "success"
});

$("[name='workLoad']").bootstrapSwitch({
	onText: "ใช่",
	onColor: "danger",
	offText: "ไม่ใช่",
	offColor: "success"
});