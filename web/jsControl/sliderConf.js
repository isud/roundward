
$("[name='sickLeave']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " คน",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='errandLeave']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " คน",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='empAccident']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " คน",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='insufMedical']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 20,
	from: 0,
	to: 20,
	step: 1,
	postfix: " รายการ",
	values: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
	grid: true
});

$("[name='patDocument']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " รายการ",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='falseProcess']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " ครั้ง",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='environment']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " รายการ",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='complain']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " ครั้ง",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='patAccident']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " คน",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='noKin']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " คน",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='financeProblem']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " ครั้ง",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='missMaterial']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 10,
	from: 0,
	to: 10,
	step: 1,
	postfix: " รายการ",
	values: [0,1,2,3,4,5,6,7,8,9,10],
	grid: true
});

$("[name='ambulance']").ionRangeSlider({
	hide_min_max: true,
	keyboard: true,
	min: 0,
	max: 20,
	from: 0,
	to: 20,
	step: 1,
	postfix: " ครั้ง",
	values: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
	grid: true
});

/*
$(".submit").click(function() {
	test = $('input:text[name=normal]').val();
	alert(test);
});
*/
