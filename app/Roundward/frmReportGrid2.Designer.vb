﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportGrid2
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportGrid2))
        Me.gcPoint = New DevExpress.XtraGrid.GridControl()
        Me.DtsetRound1 = New Roundward.dtsetRound()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colnormal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colabnormal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsickleave = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colerrandleave = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colworkLoad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colempAccident = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colinsufMedical = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpatDocument = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colfalseProcess = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colenvironment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcomplain = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpatAccident = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colnoKin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colfinanceProblem = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmissMaterial = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colambulance = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colother = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colclinic = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colround_shift = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldate_round = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDATESERV = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colclinic_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colround_shift_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colevaName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colround_point_id = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpointType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colimg_path = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colimg_path2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colimg_path3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colimg_path4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colimg_path5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.peImg_path5 = New DevExpress.XtraEditors.PictureEdit()
        Me.peImg_path3 = New DevExpress.XtraEditors.PictureEdit()
        Me.peImg_path4 = New DevExpress.XtraEditors.PictureEdit()
        Me.peImg_path2 = New DevExpress.XtraEditors.PictureEdit()
        Me.peImg_path = New DevExpress.XtraEditors.PictureEdit()
        Me.search = New DevExpress.XtraEditors.SimpleButton()
        Me.dateend = New DevExpress.XtraEditors.DateEdit()
        Me.datestart = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.layoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem1 = New DevExpress.XtraLayout.SplitterItem()
        Me.SplitterItem4 = New DevExpress.XtraLayout.SplitterItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.SplitterItem3 = New DevExpress.XtraLayout.SplitterItem()
        Me.SplitterItem5 = New DevExpress.XtraLayout.SplitterItem()
        Me.SplitterItem2 = New DevExpress.XtraLayout.SplitterItem()
        Me.DtsetRound2 = New Roundward.dtsetRound()
        Me.ss1 = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, GetType(Global.Roundward.WaitForm1), True, True, GetType(System.Windows.Forms.UserControl))
        CType(Me.gcPoint, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsetRound1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.peImg_path5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peImg_path3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peImg_path4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peImg_path2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.peImg_path.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateend.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateend.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datestart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datestart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitterItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsetRound2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gcPoint
        '
        Me.gcPoint.DataMember = "round_point"
        Me.gcPoint.DataSource = Me.DtsetRound1
        Me.gcPoint.Location = New System.Drawing.Point(12, 39)
        Me.gcPoint.MainView = Me.GridView1
        Me.gcPoint.Name = "gcPoint"
        Me.gcPoint.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemMemoEdit1})
        Me.gcPoint.Size = New System.Drawing.Size(1072, 614)
        Me.gcPoint.TabIndex = 0
        Me.gcPoint.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DtsetRound1
        '
        Me.DtsetRound1.DataSetName = "dtsetRound"
        Me.DtsetRound1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colnormal, Me.colabnormal, Me.colsickleave, Me.colerrandleave, Me.colworkLoad, Me.colempAccident, Me.colinsufMedical, Me.colpatDocument, Me.colfalseProcess, Me.colenvironment, Me.colcomplain, Me.colpatAccident, Me.colnoKin, Me.colfinanceProblem, Me.colmissMaterial, Me.colambulance, Me.colother, Me.colclinic, Me.colround_shift, Me.coldate_round, Me.colDATESERV, Me.colclinic_name, Me.colround_shift_name, Me.colevaName, Me.colround_point_id, Me.colpointType, Me.colimg_path, Me.colimg_path2, Me.colimg_path3, Me.colimg_path4, Me.colimg_path5})
        Me.GridView1.GridControl = Me.gcPoint
        Me.GridView1.Name = "GridView1"
        '
        'colnormal
        '
        Me.colnormal.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colnormal.AppearanceCell.Options.UseFont = True
        Me.colnormal.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colnormal.AppearanceHeader.Options.UseFont = True
        Me.colnormal.Caption = "เหตุการณ์ปกติ"
        Me.colnormal.FieldName = "normal"
        Me.colnormal.Name = "colnormal"
        Me.colnormal.OptionsColumn.AllowEdit = False
        Me.colnormal.OptionsColumn.AllowFocus = False
        Me.colnormal.Visible = True
        Me.colnormal.VisibleIndex = 0
        '
        'colabnormal
        '
        Me.colabnormal.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colabnormal.AppearanceCell.Options.UseFont = True
        Me.colabnormal.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colabnormal.AppearanceHeader.Options.UseFont = True
        Me.colabnormal.Caption = "เหตุการณ์ไม่ปกติ"
        Me.colabnormal.FieldName = "abnormal"
        Me.colabnormal.Name = "colabnormal"
        Me.colabnormal.OptionsColumn.AllowEdit = False
        Me.colabnormal.OptionsColumn.AllowFocus = False
        Me.colabnormal.Visible = True
        Me.colabnormal.VisibleIndex = 1
        '
        'colsickleave
        '
        Me.colsickleave.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colsickleave.AppearanceCell.Options.UseFont = True
        Me.colsickleave.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colsickleave.AppearanceHeader.Options.UseFont = True
        Me.colsickleave.Caption = "ลาป่วย"
        Me.colsickleave.FieldName = "sickleave"
        Me.colsickleave.Name = "colsickleave"
        Me.colsickleave.OptionsColumn.AllowEdit = False
        Me.colsickleave.OptionsColumn.AllowFocus = False
        Me.colsickleave.Visible = True
        Me.colsickleave.VisibleIndex = 2
        '
        'colerrandleave
        '
        Me.colerrandleave.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colerrandleave.AppearanceCell.Options.UseFont = True
        Me.colerrandleave.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colerrandleave.AppearanceHeader.Options.UseFont = True
        Me.colerrandleave.Caption = "ลากิจ"
        Me.colerrandleave.FieldName = "errandleave"
        Me.colerrandleave.Name = "colerrandleave"
        Me.colerrandleave.OptionsColumn.AllowEdit = False
        Me.colerrandleave.OptionsColumn.AllowFocus = False
        Me.colerrandleave.Visible = True
        Me.colerrandleave.VisibleIndex = 3
        '
        'colworkLoad
        '
        Me.colworkLoad.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colworkLoad.AppearanceCell.Options.UseFont = True
        Me.colworkLoad.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colworkLoad.AppearanceHeader.Options.UseFont = True
        Me.colworkLoad.Caption = "Work Load"
        Me.colworkLoad.FieldName = "workLoad"
        Me.colworkLoad.Name = "colworkLoad"
        Me.colworkLoad.OptionsColumn.AllowEdit = False
        Me.colworkLoad.OptionsColumn.AllowFocus = False
        Me.colworkLoad.Visible = True
        Me.colworkLoad.VisibleIndex = 4
        '
        'colempAccident
        '
        Me.colempAccident.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colempAccident.AppearanceCell.Options.UseFont = True
        Me.colempAccident.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colempAccident.AppearanceHeader.Options.UseFont = True
        Me.colempAccident.Caption = "อุบัติเหตุพนักงาน"
        Me.colempAccident.FieldName = "empAccident"
        Me.colempAccident.Name = "colempAccident"
        Me.colempAccident.OptionsColumn.AllowEdit = False
        Me.colempAccident.OptionsColumn.AllowFocus = False
        Me.colempAccident.Visible = True
        Me.colempAccident.VisibleIndex = 5
        '
        'colinsufMedical
        '
        Me.colinsufMedical.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colinsufMedical.AppearanceCell.Options.UseFont = True
        Me.colinsufMedical.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colinsufMedical.AppearanceHeader.Options.UseFont = True
        Me.colinsufMedical.Caption = "เครื่องมือ/เวชภัณฑ์ ไม่เพียงพอ"
        Me.colinsufMedical.FieldName = "insufMedical"
        Me.colinsufMedical.Name = "colinsufMedical"
        Me.colinsufMedical.OptionsColumn.AllowEdit = False
        Me.colinsufMedical.OptionsColumn.AllowFocus = False
        Me.colinsufMedical.Visible = True
        Me.colinsufMedical.VisibleIndex = 6
        '
        'colpatDocument
        '
        Me.colpatDocument.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpatDocument.AppearanceCell.Options.UseFont = True
        Me.colpatDocument.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpatDocument.AppearanceHeader.Options.UseFont = True
        Me.colpatDocument.Caption = "เอกสารผู้ป่วย"
        Me.colpatDocument.FieldName = "patDocument"
        Me.colpatDocument.Name = "colpatDocument"
        Me.colpatDocument.OptionsColumn.AllowEdit = False
        Me.colpatDocument.OptionsColumn.AllowFocus = False
        Me.colpatDocument.Visible = True
        Me.colpatDocument.VisibleIndex = 7
        '
        'colfalseProcess
        '
        Me.colfalseProcess.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colfalseProcess.AppearanceCell.Options.UseFont = True
        Me.colfalseProcess.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colfalseProcess.AppearanceHeader.Options.UseFont = True
        Me.colfalseProcess.Caption = "ระบบงานผิดพลาด"
        Me.colfalseProcess.FieldName = "falseProcess"
        Me.colfalseProcess.Name = "colfalseProcess"
        Me.colfalseProcess.OptionsColumn.AllowEdit = False
        Me.colfalseProcess.OptionsColumn.AllowFocus = False
        Me.colfalseProcess.Visible = True
        Me.colfalseProcess.VisibleIndex = 8
        '
        'colenvironment
        '
        Me.colenvironment.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colenvironment.AppearanceCell.Options.UseFont = True
        Me.colenvironment.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colenvironment.AppearanceHeader.Options.UseFont = True
        Me.colenvironment.Caption = "สิ่งแวดล้อม แสง/สี/เสียง"
        Me.colenvironment.FieldName = "environment"
        Me.colenvironment.Name = "colenvironment"
        Me.colenvironment.OptionsColumn.AllowEdit = False
        Me.colenvironment.OptionsColumn.AllowFocus = False
        Me.colenvironment.Visible = True
        Me.colenvironment.VisibleIndex = 9
        '
        'colcomplain
        '
        Me.colcomplain.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colcomplain.AppearanceCell.Options.UseFont = True
        Me.colcomplain.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colcomplain.AppearanceHeader.Options.UseFont = True
        Me.colcomplain.Caption = "ลูกค้าร้องเรียน"
        Me.colcomplain.FieldName = "complain"
        Me.colcomplain.Name = "colcomplain"
        Me.colcomplain.OptionsColumn.AllowEdit = False
        Me.colcomplain.OptionsColumn.AllowFocus = False
        Me.colcomplain.Visible = True
        Me.colcomplain.VisibleIndex = 10
        '
        'colpatAccident
        '
        Me.colpatAccident.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpatAccident.AppearanceCell.Options.UseFont = True
        Me.colpatAccident.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpatAccident.AppearanceHeader.Options.UseFont = True
        Me.colpatAccident.Caption = "อุบัติเหตุผู้ป่วย"
        Me.colpatAccident.FieldName = "patAccident"
        Me.colpatAccident.Name = "colpatAccident"
        Me.colpatAccident.OptionsColumn.AllowEdit = False
        Me.colpatAccident.OptionsColumn.AllowFocus = False
        Me.colpatAccident.Visible = True
        Me.colpatAccident.VisibleIndex = 11
        '
        'colnoKin
        '
        Me.colnoKin.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colnoKin.AppearanceCell.Options.UseFont = True
        Me.colnoKin.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colnoKin.AppearanceHeader.Options.UseFont = True
        Me.colnoKin.Caption = "ไม่มีญาติ"
        Me.colnoKin.FieldName = "noKin"
        Me.colnoKin.Name = "colnoKin"
        Me.colnoKin.OptionsColumn.AllowEdit = False
        Me.colnoKin.OptionsColumn.AllowFocus = False
        Me.colnoKin.Visible = True
        Me.colnoKin.VisibleIndex = 12
        '
        'colfinanceProblem
        '
        Me.colfinanceProblem.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colfinanceProblem.AppearanceCell.Options.UseFont = True
        Me.colfinanceProblem.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colfinanceProblem.AppearanceHeader.Options.UseFont = True
        Me.colfinanceProblem.Caption = "พิจารณาปัญหาทางการเงิน"
        Me.colfinanceProblem.FieldName = "financeProblem"
        Me.colfinanceProblem.Name = "colfinanceProblem"
        Me.colfinanceProblem.OptionsColumn.AllowEdit = False
        Me.colfinanceProblem.OptionsColumn.AllowFocus = False
        Me.colfinanceProblem.Visible = True
        Me.colfinanceProblem.VisibleIndex = 13
        '
        'colmissMaterial
        '
        Me.colmissMaterial.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colmissMaterial.AppearanceCell.Options.UseFont = True
        Me.colmissMaterial.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colmissMaterial.AppearanceHeader.Options.UseFont = True
        Me.colmissMaterial.Caption = "ของสูญหาย"
        Me.colmissMaterial.FieldName = "missMaterial"
        Me.colmissMaterial.Name = "colmissMaterial"
        Me.colmissMaterial.OptionsColumn.AllowEdit = False
        Me.colmissMaterial.OptionsColumn.AllowFocus = False
        Me.colmissMaterial.Visible = True
        Me.colmissMaterial.VisibleIndex = 14
        '
        'colambulance
        '
        Me.colambulance.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colambulance.AppearanceCell.Options.UseFont = True
        Me.colambulance.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colambulance.AppearanceHeader.Options.UseFont = True
        Me.colambulance.Caption = "รถพยาบาล"
        Me.colambulance.FieldName = "ambulance"
        Me.colambulance.Name = "colambulance"
        Me.colambulance.OptionsColumn.AllowEdit = False
        Me.colambulance.OptionsColumn.AllowFocus = False
        Me.colambulance.Visible = True
        Me.colambulance.VisibleIndex = 15
        '
        'colother
        '
        Me.colother.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colother.AppearanceCell.Options.UseFont = True
        Me.colother.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colother.AppearanceHeader.Options.UseFont = True
        Me.colother.Caption = "อื่นๆ"
        Me.colother.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colother.FieldName = "other"
        Me.colother.Name = "colother"
        Me.colother.OptionsColumn.AllowEdit = False
        Me.colother.OptionsColumn.AllowFocus = False
        Me.colother.Visible = True
        Me.colother.VisibleIndex = 16
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'colclinic
        '
        Me.colclinic.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colclinic.AppearanceCell.Options.UseFont = True
        Me.colclinic.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colclinic.AppearanceHeader.Options.UseFont = True
        Me.colclinic.Caption = "idClinic"
        Me.colclinic.FieldName = "clinic"
        Me.colclinic.Name = "colclinic"
        Me.colclinic.OptionsColumn.AllowEdit = False
        Me.colclinic.OptionsColumn.AllowFocus = False
        '
        'colround_shift
        '
        Me.colround_shift.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colround_shift.AppearanceCell.Options.UseFont = True
        Me.colround_shift.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colround_shift.AppearanceHeader.Options.UseFont = True
        Me.colround_shift.Caption = "idRoundShift"
        Me.colround_shift.FieldName = "round_shift"
        Me.colround_shift.Name = "colround_shift"
        Me.colround_shift.OptionsColumn.AllowEdit = False
        Me.colround_shift.OptionsColumn.AllowFocus = False
        '
        'coldate_round
        '
        Me.coldate_round.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldate_round.AppearanceCell.Options.UseFont = True
        Me.coldate_round.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.coldate_round.AppearanceHeader.Options.UseFont = True
        Me.coldate_round.FieldName = "date_round"
        Me.coldate_round.Name = "coldate_round"
        Me.coldate_round.OptionsColumn.AllowEdit = False
        Me.coldate_round.OptionsColumn.AllowFocus = False
        '
        'colDATESERV
        '
        Me.colDATESERV.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colDATESERV.AppearanceCell.Options.UseFont = True
        Me.colDATESERV.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colDATESERV.AppearanceHeader.Options.UseFont = True
        Me.colDATESERV.FieldName = "DATESERV"
        Me.colDATESERV.Name = "colDATESERV"
        Me.colDATESERV.OptionsColumn.AllowEdit = False
        Me.colDATESERV.OptionsColumn.AllowFocus = False
        Me.colDATESERV.Visible = True
        Me.colDATESERV.VisibleIndex = 18
        '
        'colclinic_name
        '
        Me.colclinic_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colclinic_name.AppearanceCell.Options.UseFont = True
        Me.colclinic_name.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colclinic_name.AppearanceHeader.Options.UseFont = True
        Me.colclinic_name.Caption = "แผนก"
        Me.colclinic_name.FieldName = "clinic_name"
        Me.colclinic_name.Name = "colclinic_name"
        Me.colclinic_name.OptionsColumn.AllowEdit = False
        Me.colclinic_name.OptionsColumn.AllowFocus = False
        Me.colclinic_name.Visible = True
        Me.colclinic_name.VisibleIndex = 19
        '
        'colround_shift_name
        '
        Me.colround_shift_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colround_shift_name.AppearanceCell.Options.UseFont = True
        Me.colround_shift_name.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colround_shift_name.AppearanceHeader.Options.UseFont = True
        Me.colround_shift_name.Caption = "เวร"
        Me.colround_shift_name.FieldName = "round_shift_name"
        Me.colround_shift_name.Name = "colround_shift_name"
        Me.colround_shift_name.OptionsColumn.AllowEdit = False
        Me.colround_shift_name.OptionsColumn.AllowFocus = False
        Me.colround_shift_name.Visible = True
        Me.colround_shift_name.VisibleIndex = 20
        '
        'colevaName
        '
        Me.colevaName.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colevaName.AppearanceCell.Options.UseFont = True
        Me.colevaName.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colevaName.AppearanceHeader.Options.UseFont = True
        Me.colevaName.Caption = "ผู้ประเมิน"
        Me.colevaName.FieldName = "evaName"
        Me.colevaName.Name = "colevaName"
        Me.colevaName.OptionsColumn.AllowEdit = False
        Me.colevaName.OptionsColumn.AllowFocus = False
        Me.colevaName.Visible = True
        Me.colevaName.VisibleIndex = 21
        '
        'colround_point_id
        '
        Me.colround_point_id.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colround_point_id.AppearanceCell.Options.UseFont = True
        Me.colround_point_id.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colround_point_id.AppearanceHeader.Options.UseFont = True
        Me.colround_point_id.Caption = "id"
        Me.colround_point_id.FieldName = "round_point_id"
        Me.colround_point_id.Name = "colround_point_id"
        Me.colround_point_id.OptionsColumn.AllowEdit = False
        Me.colround_point_id.OptionsColumn.AllowFocus = False
        '
        'colpointType
        '
        Me.colpointType.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpointType.AppearanceCell.Options.UseFont = True
        Me.colpointType.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.colpointType.AppearanceHeader.Options.UseFont = True
        Me.colpointType.Caption = "ประเภทการประเมิน"
        Me.colpointType.FieldName = "pointType"
        Me.colpointType.Name = "colpointType"
        Me.colpointType.OptionsColumn.AllowEdit = False
        Me.colpointType.OptionsColumn.AllowFocus = False
        Me.colpointType.Visible = True
        Me.colpointType.VisibleIndex = 17
        '
        'colimg_path
        '
        Me.colimg_path.FieldName = "img_path"
        Me.colimg_path.Name = "colimg_path"
        '
        'colimg_path2
        '
        Me.colimg_path2.FieldName = "img_path2"
        Me.colimg_path2.Name = "colimg_path2"
        '
        'colimg_path3
        '
        Me.colimg_path3.FieldName = "img_path3"
        Me.colimg_path3.Name = "colimg_path3"
        '
        'colimg_path4
        '
        Me.colimg_path4.FieldName = "img_path4"
        Me.colimg_path4.Name = "colimg_path4"
        '
        'colimg_path5
        '
        Me.colimg_path5.FieldName = "img_path5"
        Me.colimg_path5.Name = "colimg_path5"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.peImg_path5)
        Me.LayoutControl1.Controls.Add(Me.peImg_path3)
        Me.LayoutControl1.Controls.Add(Me.peImg_path4)
        Me.LayoutControl1.Controls.Add(Me.peImg_path2)
        Me.LayoutControl1.Controls.Add(Me.peImg_path)
        Me.LayoutControl1.Controls.Add(Me.search)
        Me.LayoutControl1.Controls.Add(Me.dateend)
        Me.LayoutControl1.Controls.Add(Me.datestart)
        Me.LayoutControl1.Controls.Add(Me.gcPoint)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1275, 101, 387, 449)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1096, 700)
        Me.LayoutControl1.TabIndex = 1
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'peImg_path5
        '
        Me.peImg_path5.Location = New System.Drawing.Point(543, 987)
        Me.peImg_path5.Name = "peImg_path5"
        Me.peImg_path5.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.peImg_path5.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.peImg_path5.Size = New System.Drawing.Size(517, 134)
        Me.peImg_path5.StyleController = Me.LayoutControl1
        Me.peImg_path5.TabIndex = 15
        '
        'peImg_path3
        '
        Me.peImg_path3.Location = New System.Drawing.Point(542, 834)
        Me.peImg_path3.Name = "peImg_path3"
        Me.peImg_path3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.peImg_path3.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.peImg_path3.Size = New System.Drawing.Size(518, 144)
        Me.peImg_path3.StyleController = Me.LayoutControl1
        Me.peImg_path3.TabIndex = 14
        '
        'peImg_path4
        '
        Me.peImg_path4.Location = New System.Drawing.Point(12, 987)
        Me.peImg_path4.Name = "peImg_path4"
        Me.peImg_path4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.peImg_path4.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.peImg_path4.Size = New System.Drawing.Size(522, 134)
        Me.peImg_path4.StyleController = Me.LayoutControl1
        Me.peImg_path4.TabIndex = 13
        '
        'peImg_path2
        '
        Me.peImg_path2.Location = New System.Drawing.Point(12, 834)
        Me.peImg_path2.Name = "peImg_path2"
        Me.peImg_path2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.peImg_path2.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.peImg_path2.Size = New System.Drawing.Size(521, 144)
        Me.peImg_path2.StyleController = Me.LayoutControl1
        Me.peImg_path2.TabIndex = 11
        '
        'peImg_path
        '
        Me.peImg_path.Location = New System.Drawing.Point(12, 662)
        Me.peImg_path.Name = "peImg_path"
        Me.peImg_path.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.peImg_path.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze
        Me.peImg_path.Size = New System.Drawing.Size(1048, 163)
        Me.peImg_path.StyleController = Me.LayoutControl1
        Me.peImg_path.TabIndex = 9
        '
        'search
        '
        Me.search.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.search.Appearance.Options.UseFont = True
        Me.search.Image = CType(resources.GetObject("search.Image"), System.Drawing.Image)
        Me.search.Location = New System.Drawing.Point(942, 12)
        Me.search.Name = "search"
        Me.search.Size = New System.Drawing.Size(142, 23)
        Me.search.StyleController = Me.LayoutControl1
        Me.search.TabIndex = 8
        Me.search.Text = "ค้นหา"
        '
        'dateend
        '
        Me.dateend.EditValue = Nothing
        Me.dateend.Location = New System.Drawing.Point(728, 12)
        Me.dateend.Name = "dateend"
        Me.dateend.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateend.Properties.Appearance.Options.UseFont = True
        Me.dateend.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateend.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateend.Size = New System.Drawing.Size(210, 22)
        Me.dateend.StyleController = Me.LayoutControl1
        Me.dateend.TabIndex = 5
        '
        'datestart
        '
        Me.datestart.EditValue = Nothing
        Me.datestart.Location = New System.Drawing.Point(434, 12)
        Me.datestart.Name = "datestart"
        Me.datestart.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.datestart.Properties.Appearance.Options.UseFont = True
        Me.datestart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.datestart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.datestart.Size = New System.Drawing.Size(211, 22)
        Me.datestart.StyleController = Me.LayoutControl1
        Me.datestart.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.layoutControlGroup6, Me.SplitterItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1096, 700)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.gcPoint
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 27)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1076, 618)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.datestart
        Me.LayoutControlItem2.Location = New System.Drawing.Point(382, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(255, 27)
        Me.LayoutControlItem2.Text = "วันที่"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(37, 16)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem3.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem3.Control = Me.dateend
        Me.LayoutControlItem3.Location = New System.Drawing.Point(676, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(254, 27)
        Me.LayoutControlItem3.Text = "ถึงวันที่"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(37, 16)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.search
        Me.LayoutControlItem4.Location = New System.Drawing.Point(930, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(146, 27)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(382, 27)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(637, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(39, 27)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'layoutControlGroup6
        '
        Me.layoutControlGroup6.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup6.AppearanceGroup.Options.UseFont = True
        Me.layoutControlGroup6.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.layoutControlGroup6.AppearanceItemCaption.Options.UseFont = True
        Me.layoutControlGroup6.CustomizationFormText = "layoutControlGroup6"
        Me.layoutControlGroup6.ExpandButtonVisible = True
        Me.layoutControlGroup6.Expanded = False
        Me.layoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5, Me.LayoutControlItem7, Me.LayoutControlItem9, Me.SplitterItem1, Me.SplitterItem4, Me.LayoutControlItem6, Me.LayoutControlItem8, Me.SplitterItem3, Me.SplitterItem5})
        Me.layoutControlGroup6.Location = New System.Drawing.Point(0, 650)
        Me.layoutControlGroup6.Name = "layoutControlGroup6"
        Me.layoutControlGroup6.Size = New System.Drawing.Size(1076, 30)
        Me.layoutControlGroup6.Text = "รูปภาพ"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.peImg_path
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(1052, 167)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.peImg_path2
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 172)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(525, 148)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.peImg_path4
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 325)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(526, 138)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'SplitterItem1
        '
        Me.SplitterItem1.AllowHotTrack = True
        Me.SplitterItem1.Location = New System.Drawing.Point(0, 167)
        Me.SplitterItem1.Name = "SplitterItem1"
        Me.SplitterItem1.Size = New System.Drawing.Size(1052, 5)
        '
        'SplitterItem4
        '
        Me.SplitterItem4.AllowHotTrack = True
        Me.SplitterItem4.Location = New System.Drawing.Point(0, 320)
        Me.SplitterItem4.Name = "SplitterItem4"
        Me.SplitterItem4.Size = New System.Drawing.Size(1052, 5)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.peImg_path3
        Me.LayoutControlItem6.Location = New System.Drawing.Point(530, 172)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(522, 148)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.peImg_path5
        Me.LayoutControlItem8.Location = New System.Drawing.Point(531, 325)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(521, 138)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'SplitterItem3
        '
        Me.SplitterItem3.AllowHotTrack = True
        Me.SplitterItem3.Location = New System.Drawing.Point(525, 172)
        Me.SplitterItem3.Name = "SplitterItem3"
        Me.SplitterItem3.Size = New System.Drawing.Size(5, 148)
        '
        'SplitterItem5
        '
        Me.SplitterItem5.AllowHotTrack = True
        Me.SplitterItem5.Location = New System.Drawing.Point(526, 325)
        Me.SplitterItem5.Name = "SplitterItem5"
        Me.SplitterItem5.Size = New System.Drawing.Size(5, 138)
        '
        'SplitterItem2
        '
        Me.SplitterItem2.AllowHotTrack = True
        Me.SplitterItem2.Location = New System.Drawing.Point(0, 645)
        Me.SplitterItem2.Name = "SplitterItem2"
        Me.SplitterItem2.Size = New System.Drawing.Size(1076, 5)
        '
        'DtsetRound2
        '
        Me.DtsetRound2.DataSetName = "dtsetRound"
        Me.DtsetRound2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ss1
        '
        Me.ss1.ClosingDelay = 500
        '
        'frmReportGrid2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmReportGrid2"
        Me.Size = New System.Drawing.Size(1096, 700)
        CType(Me.gcPoint, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsetRound1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.peImg_path5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peImg_path3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peImg_path4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peImg_path2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.peImg_path.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateend.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateend.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datestart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datestart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitterItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsetRound2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents gcPoint As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents search As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents dateend As DevExpress.XtraEditors.DateEdit
    Friend WithEvents datestart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents DtsetRound1 As dtsetRound
    Friend WithEvents colnormal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colabnormal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsickleave As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colerrandleave As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colworkLoad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colempAccident As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colinsufMedical As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpatDocument As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colfalseProcess As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colenvironment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcomplain As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpatAccident As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colnoKin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colfinanceProblem As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmissMaterial As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colother As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colclinic As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colround_shift As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldate_round As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDATESERV As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colclinic_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colround_shift_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colevaName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colround_point_id As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colambulance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colpointType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents colimg_path As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colimg_path2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colimg_path3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colimg_path4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colimg_path5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DtsetRound2 As Roundward.dtsetRound
    Friend WithEvents layoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents SplitterItem2 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents peImg_path As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents peImg_path4 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents peImg_path2 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents peImg_path5 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents peImg_path3 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents SplitterItem1 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents SplitterItem4 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SplitterItem3 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents SplitterItem5 As DevExpress.XtraLayout.SplitterItem
    Friend WithEvents ss1 As DevExpress.XtraSplashScreen.SplashScreenManager
End Class
