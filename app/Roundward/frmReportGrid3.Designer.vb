﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportGrid3
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportGrid3))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.btnSearch = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.DtsetRound1 = New Roundward.dtsetRound()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colnormal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colabnormal = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colsickleave = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colerrandleave = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colworkLoad = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colempAccident = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colinsufMedical = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpatDocument = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colfalseProcess = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colenvironment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcomplain = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpatAccident = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colnoKin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colfinanceProblem = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colmissMaterial = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colother = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colclinic = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colround_shift = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.coldate_round = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDATESERV = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colclinic_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colround_shift_name = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colevaName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colround_point_id = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colambulance = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colpointType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.dateEnd = New DevExpress.XtraEditors.DateEdit()
        Me.dateStart = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.SimpleLabelItem1 = New DevExpress.XtraLayout.SimpleLabelItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsetRound1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SimpleLabelItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.btnSearch)
        Me.LayoutControl1.Controls.Add(Me.GridControl1)
        Me.LayoutControl1.Controls.Add(Me.dateEnd)
        Me.LayoutControl1.Controls.Add(Me.dateStart)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(570, 302, 558, 552)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'btnSearch
        '
        Me.btnSearch.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSearch.Appearance.Options.UseFont = True
        Me.btnSearch.Image = CType(resources.GetObject("btnSearch.Image"), System.Drawing.Image)
        Me.btnSearch.Location = New System.Drawing.Point(1128, 24)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(148, 23)
        Me.btnSearch.StyleController = Me.LayoutControl1
        Me.btnSearch.TabIndex = 7
        Me.btnSearch.Text = "ค้นหา"
        '
        'GridControl1
        '
        Me.GridControl1.DataMember = "round_point"
        Me.GridControl1.DataSource = Me.DtsetRound1
        Me.GridControl1.Location = New System.Drawing.Point(4, 51)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemMemoEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(1272, 745)
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'DtsetRound1
        '
        Me.DtsetRound1.DataSetName = "dtsetRound"
        Me.DtsetRound1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colnormal, Me.colabnormal, Me.colsickleave, Me.colerrandleave, Me.colworkLoad, Me.colempAccident, Me.colinsufMedical, Me.colpatDocument, Me.colfalseProcess, Me.colenvironment, Me.colcomplain, Me.colpatAccident, Me.colnoKin, Me.colfinanceProblem, Me.colmissMaterial, Me.colother, Me.colclinic, Me.colround_shift, Me.coldate_round, Me.colDATESERV, Me.colclinic_name, Me.colround_shift_name, Me.colevaName, Me.colround_point_id, Me.colambulance, Me.colpointType})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsView.RowAutoHeight = True
        '
        'colnormal
        '
        Me.colnormal.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colnormal.AppearanceCell.Options.UseFont = True
        Me.colnormal.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colnormal.AppearanceHeader.Options.UseFont = True
        Me.colnormal.FieldName = "normal"
        Me.colnormal.Name = "colnormal"
        Me.colnormal.OptionsColumn.AllowEdit = False
        Me.colnormal.OptionsColumn.AllowFocus = False
        '
        'colabnormal
        '
        Me.colabnormal.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colabnormal.AppearanceCell.Options.UseFont = True
        Me.colabnormal.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colabnormal.AppearanceHeader.Options.UseFont = True
        Me.colabnormal.FieldName = "abnormal"
        Me.colabnormal.Name = "colabnormal"
        Me.colabnormal.OptionsColumn.AllowEdit = False
        Me.colabnormal.OptionsColumn.AllowFocus = False
        '
        'colsickleave
        '
        Me.colsickleave.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colsickleave.AppearanceCell.Options.UseFont = True
        Me.colsickleave.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colsickleave.AppearanceHeader.Options.UseFont = True
        Me.colsickleave.FieldName = "sickleave"
        Me.colsickleave.Name = "colsickleave"
        Me.colsickleave.OptionsColumn.AllowEdit = False
        Me.colsickleave.OptionsColumn.AllowFocus = False
        '
        'colerrandleave
        '
        Me.colerrandleave.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colerrandleave.AppearanceCell.Options.UseFont = True
        Me.colerrandleave.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colerrandleave.AppearanceHeader.Options.UseFont = True
        Me.colerrandleave.FieldName = "errandleave"
        Me.colerrandleave.Name = "colerrandleave"
        Me.colerrandleave.OptionsColumn.AllowEdit = False
        Me.colerrandleave.OptionsColumn.AllowFocus = False
        '
        'colworkLoad
        '
        Me.colworkLoad.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colworkLoad.AppearanceCell.Options.UseFont = True
        Me.colworkLoad.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colworkLoad.AppearanceHeader.Options.UseFont = True
        Me.colworkLoad.FieldName = "workLoad"
        Me.colworkLoad.Name = "colworkLoad"
        Me.colworkLoad.OptionsColumn.AllowEdit = False
        Me.colworkLoad.OptionsColumn.AllowFocus = False
        '
        'colempAccident
        '
        Me.colempAccident.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colempAccident.AppearanceCell.Options.UseFont = True
        Me.colempAccident.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colempAccident.AppearanceHeader.Options.UseFont = True
        Me.colempAccident.FieldName = "empAccident"
        Me.colempAccident.Name = "colempAccident"
        Me.colempAccident.OptionsColumn.AllowEdit = False
        Me.colempAccident.OptionsColumn.AllowFocus = False
        '
        'colinsufMedical
        '
        Me.colinsufMedical.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colinsufMedical.AppearanceCell.Options.UseFont = True
        Me.colinsufMedical.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colinsufMedical.AppearanceHeader.Options.UseFont = True
        Me.colinsufMedical.FieldName = "insufMedical"
        Me.colinsufMedical.Name = "colinsufMedical"
        Me.colinsufMedical.OptionsColumn.AllowEdit = False
        Me.colinsufMedical.OptionsColumn.AllowFocus = False
        '
        'colpatDocument
        '
        Me.colpatDocument.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colpatDocument.AppearanceCell.Options.UseFont = True
        Me.colpatDocument.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colpatDocument.AppearanceHeader.Options.UseFont = True
        Me.colpatDocument.FieldName = "patDocument"
        Me.colpatDocument.Name = "colpatDocument"
        Me.colpatDocument.OptionsColumn.AllowEdit = False
        Me.colpatDocument.OptionsColumn.AllowFocus = False
        '
        'colfalseProcess
        '
        Me.colfalseProcess.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colfalseProcess.AppearanceCell.Options.UseFont = True
        Me.colfalseProcess.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colfalseProcess.AppearanceHeader.Options.UseFont = True
        Me.colfalseProcess.FieldName = "falseProcess"
        Me.colfalseProcess.Name = "colfalseProcess"
        Me.colfalseProcess.OptionsColumn.AllowEdit = False
        Me.colfalseProcess.OptionsColumn.AllowFocus = False
        '
        'colenvironment
        '
        Me.colenvironment.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colenvironment.AppearanceCell.Options.UseFont = True
        Me.colenvironment.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colenvironment.AppearanceHeader.Options.UseFont = True
        Me.colenvironment.FieldName = "environment"
        Me.colenvironment.Name = "colenvironment"
        Me.colenvironment.OptionsColumn.AllowEdit = False
        Me.colenvironment.OptionsColumn.AllowFocus = False
        '
        'colcomplain
        '
        Me.colcomplain.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colcomplain.AppearanceCell.Options.UseFont = True
        Me.colcomplain.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colcomplain.AppearanceHeader.Options.UseFont = True
        Me.colcomplain.FieldName = "complain"
        Me.colcomplain.Name = "colcomplain"
        Me.colcomplain.OptionsColumn.AllowEdit = False
        Me.colcomplain.OptionsColumn.AllowFocus = False
        '
        'colpatAccident
        '
        Me.colpatAccident.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colpatAccident.AppearanceCell.Options.UseFont = True
        Me.colpatAccident.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colpatAccident.AppearanceHeader.Options.UseFont = True
        Me.colpatAccident.FieldName = "patAccident"
        Me.colpatAccident.Name = "colpatAccident"
        Me.colpatAccident.OptionsColumn.AllowEdit = False
        Me.colpatAccident.OptionsColumn.AllowFocus = False
        '
        'colnoKin
        '
        Me.colnoKin.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colnoKin.AppearanceCell.Options.UseFont = True
        Me.colnoKin.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colnoKin.AppearanceHeader.Options.UseFont = True
        Me.colnoKin.FieldName = "noKin"
        Me.colnoKin.Name = "colnoKin"
        Me.colnoKin.OptionsColumn.AllowEdit = False
        Me.colnoKin.OptionsColumn.AllowFocus = False
        '
        'colfinanceProblem
        '
        Me.colfinanceProblem.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colfinanceProblem.AppearanceCell.Options.UseFont = True
        Me.colfinanceProblem.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colfinanceProblem.AppearanceHeader.Options.UseFont = True
        Me.colfinanceProblem.FieldName = "financeProblem"
        Me.colfinanceProblem.Name = "colfinanceProblem"
        Me.colfinanceProblem.OptionsColumn.AllowEdit = False
        Me.colfinanceProblem.OptionsColumn.AllowFocus = False
        '
        'colmissMaterial
        '
        Me.colmissMaterial.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colmissMaterial.AppearanceCell.Options.UseFont = True
        Me.colmissMaterial.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colmissMaterial.AppearanceHeader.Options.UseFont = True
        Me.colmissMaterial.FieldName = "missMaterial"
        Me.colmissMaterial.Name = "colmissMaterial"
        Me.colmissMaterial.OptionsColumn.AllowEdit = False
        Me.colmissMaterial.OptionsColumn.AllowFocus = False
        '
        'colother
        '
        Me.colother.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colother.AppearanceCell.Options.UseFont = True
        Me.colother.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colother.AppearanceHeader.Options.UseFont = True
        Me.colother.Caption = "รายละเอียด"
        Me.colother.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colother.FieldName = "other"
        Me.colother.Name = "colother"
        Me.colother.OptionsColumn.AllowEdit = False
        Me.colother.OptionsColumn.AllowFocus = False
        Me.colother.Visible = True
        Me.colother.VisibleIndex = 3
        Me.colother.Width = 704
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'colclinic
        '
        Me.colclinic.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colclinic.AppearanceCell.Options.UseFont = True
        Me.colclinic.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colclinic.AppearanceHeader.Options.UseFont = True
        Me.colclinic.FieldName = "clinic"
        Me.colclinic.Name = "colclinic"
        Me.colclinic.OptionsColumn.AllowEdit = False
        Me.colclinic.OptionsColumn.AllowFocus = False
        '
        'colround_shift
        '
        Me.colround_shift.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colround_shift.AppearanceCell.Options.UseFont = True
        Me.colround_shift.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colround_shift.AppearanceHeader.Options.UseFont = True
        Me.colround_shift.FieldName = "round_shift"
        Me.colround_shift.Name = "colround_shift"
        Me.colround_shift.OptionsColumn.AllowEdit = False
        Me.colround_shift.OptionsColumn.AllowFocus = False
        '
        'coldate_round
        '
        Me.coldate_round.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.coldate_round.AppearanceCell.Options.UseFont = True
        Me.coldate_round.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.coldate_round.AppearanceHeader.Options.UseFont = True
        Me.coldate_round.FieldName = "date_round"
        Me.coldate_round.Name = "coldate_round"
        Me.coldate_round.OptionsColumn.AllowEdit = False
        Me.coldate_round.OptionsColumn.AllowFocus = False
        '
        'colDATESERV
        '
        Me.colDATESERV.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDATESERV.AppearanceCell.Options.UseFont = True
        Me.colDATESERV.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colDATESERV.AppearanceHeader.Options.UseFont = True
        Me.colDATESERV.Caption = "วันที่ประเมิน"
        Me.colDATESERV.FieldName = "DATESERV"
        Me.colDATESERV.Name = "colDATESERV"
        Me.colDATESERV.OptionsColumn.AllowEdit = False
        Me.colDATESERV.OptionsColumn.AllowFocus = False
        Me.colDATESERV.Visible = True
        Me.colDATESERV.VisibleIndex = 2
        Me.colDATESERV.Width = 97
        '
        'colclinic_name
        '
        Me.colclinic_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colclinic_name.AppearanceCell.Options.UseFont = True
        Me.colclinic_name.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colclinic_name.AppearanceHeader.Options.UseFont = True
        Me.colclinic_name.Caption = "แผนก"
        Me.colclinic_name.FieldName = "clinic_name"
        Me.colclinic_name.Name = "colclinic_name"
        Me.colclinic_name.OptionsColumn.AllowEdit = False
        Me.colclinic_name.OptionsColumn.AllowFocus = False
        Me.colclinic_name.Visible = True
        Me.colclinic_name.VisibleIndex = 0
        Me.colclinic_name.Width = 87
        '
        'colround_shift_name
        '
        Me.colround_shift_name.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colround_shift_name.AppearanceCell.Options.UseFont = True
        Me.colround_shift_name.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colround_shift_name.AppearanceHeader.Options.UseFont = True
        Me.colround_shift_name.Caption = "เวร"
        Me.colround_shift_name.FieldName = "round_shift_name"
        Me.colround_shift_name.Name = "colround_shift_name"
        Me.colround_shift_name.OptionsColumn.AllowEdit = False
        Me.colround_shift_name.OptionsColumn.AllowFocus = False
        Me.colround_shift_name.Visible = True
        Me.colround_shift_name.VisibleIndex = 1
        Me.colround_shift_name.Width = 96
        '
        'colevaName
        '
        Me.colevaName.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colevaName.AppearanceCell.Options.UseFont = True
        Me.colevaName.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colevaName.AppearanceHeader.Options.UseFont = True
        Me.colevaName.Caption = "ผู้ประเมิน"
        Me.colevaName.FieldName = "evaName"
        Me.colevaName.Name = "colevaName"
        Me.colevaName.OptionsColumn.AllowEdit = False
        Me.colevaName.OptionsColumn.AllowFocus = False
        Me.colevaName.Visible = True
        Me.colevaName.VisibleIndex = 4
        Me.colevaName.Width = 196
        '
        'colround_point_id
        '
        Me.colround_point_id.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colround_point_id.AppearanceCell.Options.UseFont = True
        Me.colround_point_id.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colround_point_id.AppearanceHeader.Options.UseFont = True
        Me.colround_point_id.FieldName = "round_point_id"
        Me.colround_point_id.Name = "colround_point_id"
        Me.colround_point_id.OptionsColumn.AllowEdit = False
        Me.colround_point_id.OptionsColumn.AllowFocus = False
        '
        'colambulance
        '
        Me.colambulance.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colambulance.AppearanceCell.Options.UseFont = True
        Me.colambulance.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colambulance.AppearanceHeader.Options.UseFont = True
        Me.colambulance.FieldName = "ambulance"
        Me.colambulance.Name = "colambulance"
        Me.colambulance.OptionsColumn.AllowEdit = False
        Me.colambulance.OptionsColumn.AllowFocus = False
        '
        'colpointType
        '
        Me.colpointType.AppearanceCell.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colpointType.AppearanceCell.Options.UseFont = True
        Me.colpointType.AppearanceHeader.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colpointType.AppearanceHeader.Options.UseFont = True
        Me.colpointType.FieldName = "pointType"
        Me.colpointType.Name = "colpointType"
        Me.colpointType.OptionsColumn.AllowEdit = False
        Me.colpointType.OptionsColumn.AllowFocus = False
        Me.colpointType.Width = 83
        '
        'dateEnd
        '
        Me.dateEnd.EditValue = Nothing
        Me.dateEnd.Location = New System.Drawing.Point(910, 24)
        Me.dateEnd.Name = "dateEnd"
        Me.dateEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateEnd.Properties.Appearance.Options.UseFont = True
        Me.dateEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateEnd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateEnd.Size = New System.Drawing.Size(214, 22)
        Me.dateEnd.StyleController = Me.LayoutControl1
        Me.dateEnd.TabIndex = 5
        '
        'dateStart
        '
        Me.dateStart.EditValue = Nothing
        Me.dateStart.Location = New System.Drawing.Point(589, 24)
        Me.dateStart.Name = "dateStart"
        Me.dateStart.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dateStart.Properties.Appearance.Options.UseFont = True
        Me.dateStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateStart.Size = New System.Drawing.Size(199, 22)
        Me.dateStart.StyleController = Me.LayoutControl1
        Me.dateStart.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.SimpleLabelItem1, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.EmptySpaceItem1, Me.LayoutControlItem4, Me.EmptySpaceItem2})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Padding = New DevExpress.XtraLayout.Utils.Padding(2, 2, 2, 2)
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1280, 800)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'SimpleLabelItem1
        '
        Me.SimpleLabelItem1.AllowHotTrack = False
        Me.SimpleLabelItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SimpleLabelItem1.AppearanceItemCaption.Options.UseFont = True
        Me.SimpleLabelItem1.Location = New System.Drawing.Point(0, 0)
        Me.SimpleLabelItem1.Name = "SimpleLabelItem1"
        Me.SimpleLabelItem1.Size = New System.Drawing.Size(1276, 20)
        Me.SimpleLabelItem1.Text = "รายงานการ Complain"
        Me.SimpleLabelItem1.TextSize = New System.Drawing.Size(130, 16)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem1.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem1.Control = Me.dateStart
        Me.LayoutControlItem1.Location = New System.Drawing.Point(527, 20)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(261, 27)
        Me.LayoutControlItem1.Text = "ตั้งแต่วันที่"
        Me.LayoutControlItem1.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(53, 16)
        Me.LayoutControlItem1.TextToControlDistance = 5
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlItem2.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem2.Control = Me.dateEnd
        Me.LayoutControlItem2.Location = New System.Drawing.Point(864, 20)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(260, 27)
        Me.LayoutControlItem2.Text = "ถึงวันที่"
        Me.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(37, 16)
        Me.LayoutControlItem2.TextToControlDistance = 5
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.GridControl1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 47)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(1276, 749)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(788, 20)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(76, 27)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.btnSearch
        Me.LayoutControlItem4.Location = New System.Drawing.Point(1124, 20)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(152, 27)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 20)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(527, 27)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'frmReportGrid3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmReportGrid3"
        Me.Size = New System.Drawing.Size(1280, 800)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsetRound1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SimpleLabelItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents DtsetRound1 As Roundward.dtsetRound
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colnormal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colabnormal As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colsickleave As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colerrandleave As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colworkLoad As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colempAccident As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colinsufMedical As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpatDocument As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colfalseProcess As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colenvironment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcomplain As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpatAccident As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colnoKin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colfinanceProblem As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colmissMaterial As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colother As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colclinic As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colround_shift As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents coldate_round As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDATESERV As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colclinic_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colround_shift_name As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colevaName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colround_point_id As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colambulance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colpointType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dateEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents dateStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SimpleLabelItem1 As DevExpress.XtraLayout.SimpleLabelItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents btnSearch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit

End Class
