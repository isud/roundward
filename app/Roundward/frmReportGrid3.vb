﻿Public Class frmReportGrid3
    Dim dtset As New dtsetRound
    Dim wardclass As WardClass

    Public Sub loadnew()
        datestart.EditValue = Date.Now
        dateend.EditValue = Date.Now
    End Sub

    Private Sub frmReportGrid3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadnew()
    End Sub

    Public Sub getWard()
        wardclass = New WardClass(dtset)
        wardclass.getComplain(dateStart.EditValue, dateEnd.EditValue)
        GridControl1.DataSource = dtset
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        getWard()
    End Sub
End Class
