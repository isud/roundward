﻿Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks

Public Class frmReportGrid
    Dim dtset As New dtsetRound
    Dim wardclass As WardClass
    Private Sub frmReportGrid_Load(sender As Object, e As EventArgs) Handles Me.Load
        loadnew()
    End Sub
    Public Sub loadnew()
        datestart.EditValue = Date.Now
        dateend.EditValue = Date.Now

    End Sub
    Public Sub getWard()
        wardclass = New WardClass(dtset)
        wardclass.getWardClass(datestart.EditValue, dateend.EditValue, If(RadioGroup1.SelectedIndex = 0, False, True))
        PivotGridControl1.DataSource = dtset
    End Sub
    Private Sub Search_Click(sender As Object, e As EventArgs) Handles Search.Click
        getWard()
    End Sub

End Class
