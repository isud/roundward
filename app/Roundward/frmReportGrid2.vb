﻿Imports DevExpress.XtraGrid.Views.Grid

Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Text
Imports System.Windows.Forms
Imports System.Net
Imports System.IO

Public Class frmReportGrid2
    Dim dtset As New dtsetRound
    Dim wardclass As WardClass

    Private Sub frmReportGrid_Load(sender As Object, e As EventArgs) Handles Me.Load
        loadnew()
    End Sub

    Public Sub loadnew()
        datestart.EditValue = Date.Now
        dateend.EditValue = Date.Now
    End Sub

    Public Sub getWard()
        wardclass = New WardClass(dtset)
        wardclass.getWardDetailclass(datestart.EditValue, dateend.EditValue)
        gcPoint.DataSource = dtset
    End Sub

    Private Sub Search_Click(sender As Object, e As EventArgs) Handles search.Click
        ss1.ShowWaitForm()
        getWard()
        ss1.CloseWaitForm()
    End Sub

    Private Sub GridView1_RowClick(sender As Object, e As RowClickEventArgs) Handles GridView1.RowClick
        Dim round_point_id As String = GridView1.GetDataRow(GridView1.FocusedRowHandle)("round_point_id").ToString
        getImage(round_point_id)
    End Sub

#Region "pic"

    Private Sub getImage(round_point_id As String)
        'InitializeComponent()
        Dim imgPath As String = My.Settings.PICPATH

        Dim row() As DataRow
        row = dtset.Tables("round_point").Select("round_point_id = '" & round_point_id & "'")

        If row.Count = 1 Then
            peImg_path.LoadAsync(imgPath & row(0)("img_path"))
            peImg_path2.LoadAsync(imgPath & row(0)("img_path2"))
            peImg_path3.LoadAsync(imgPath & row(0)("img_path3"))
            peImg_path4.LoadAsync(imgPath & row(0)("img_path4"))
            peImg_path5.LoadAsync(imgPath & row(0)("img_path5"))
        End If
    End Sub
#End Region

    
End Class
