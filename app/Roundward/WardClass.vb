﻿Imports System.Globalization

Public Class WardClass
    Dim dataset As dtsetRound
    Dim condb As ConnecDBRYH

    Public Sub New(ByRef dtset As dtsetRound)
        condb = ConnecDBRYH.NewConnection
        dataset = dtset
    End Sub

    Public Sub getWardClass(ByVal datstart As Date, ByVal dateend As Date, Optional checkmonth As Boolean = False)
        Dim USCulture As New System.Globalization.CultureInfo("en-US", True)
        '%d-%M-%y
        Dim sql As String
        sql = "SELECT round_point.*,mas_round_clinic.clinic_name, " & If(checkmonth = False, "date_format(date_round,'%a-%M-%y')", "date_format(date_round,'%M-%y')") & " As DATESERV ,round_shift_name FROM (SELECT * FROM round_point"

        If Convert.ToString(datstart) <> "" Then
            sql += " WHERE date_round >= '" & datstart.ToString("yyyy-MM-dd 00:00:00", USCulture) & "'"
        End If

        If Convert.ToString(dateend) <> "" Then
            If Convert.ToString(datstart) <> "" Then
                sql += " AND date_round <= '" & dateend.ToString("yyyy-MM-dd 23:59:59", USCulture) & "'"
            Else
                sql += " WHERE date_round <= '" & dateend.ToString("yyyy-MM-dd 23:59:59", USCulture) & "'"
            End If
        End If

        sql += ") AS round_point  JOIN mas_round_shift On round_point.round_shift = mas_round_shift.round_shift_id JOIN mas_round_clinic ON mas_round_clinic.clinic_id  = round_point.clinic;"
        dataset.Tables("round_point").Clear()
        condb.GetTable(sql, dataset.Tables("round_point"))
    End Sub

    Public Sub getWardDetailclass(ByVal datstart As Date, ByVal dateend As Date)
        Dim USCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim sql As String
        sql = "SELECT round_point.*, mas_round_clinic.clinic_name, date_round AS DATESERV, round_shift_name, mas_user.evaName, point_type.pointType FROM (SELECT * FROM round_point"

        If Convert.ToString(datstart) <> "" Then
            sql += " WHERE date_round >= '" & datstart.ToString("yyyy-MM-dd 00:00:00", USCulture) & "'"
        End If

        If Convert.ToString(dateend) <> "" Then
            If Convert.ToString(datstart) <> "" Then
                sql += " AND date_round <= '" & dateend.ToString("yyyy-MM-dd 23:59:59", USCulture) & "'"
            Else
                sql += " WHERE date_round <= '" & dateend.ToString("yyyy-MM-dd 23:59:59", USCulture) & "'"
            End If
        End If

        sql += ") AS round_point JOIN mas_round_shift ON round_point.round_shift = mas_round_shift.round_shift_id JOIN mas_round_clinic ON mas_round_clinic.clinic_id  = round_point.clinic JOIN (SELECT user_id, CONCAT_WS(' ', `name`, `lname`) AS 'evaName' FROM mas_user) AS mas_user ON mas_user.`user_id` = round_point.`user_id` JOIN point_type ON point_type.`pointType_id` = round_point.`pointType_id`"

        dataset.Tables("round_point").Clear()
        condb.GetTable(sql, dataset.Tables("round_point"))
    End Sub

    Public Sub getComplain(ByVal datstart As Date, ByVal dateend As Date)
        Dim USCulture As New System.Globalization.CultureInfo("en-US", True)

        Dim sql As String
        sql = "SELECT round_point.*, mas_round_clinic.clinic_name, date_round AS DATESERV, round_shift_name, mas_user.evaName, point_type.pointType FROM (SELECT * FROM round_point"

        If Convert.ToString(datstart) <> "" Then
            sql += " WHERE date_round >= '" & datstart.ToString("yyyy-MM-dd 00:00:00", USCulture) & "'"
        End If

        If Convert.ToString(dateend) <> "" Then
            If Convert.ToString(datstart) <> "" Then
                sql += " AND date_round <= '" & dateend.ToString("yyyy-MM-dd 23:59:59", USCulture) & "'"
            Else
                sql += " WHERE date_round <= '" & dateend.ToString("yyyy-MM-dd 23:59:59", USCulture) & "'"
            End If
        End If

        sql += " AND (other IS NOT NULL AND other <> '')) AS round_point JOIN mas_round_shift ON round_point.round_shift = mas_round_shift.round_shift_id JOIN mas_round_clinic ON mas_round_clinic.clinic_id  = round_point.clinic JOIN (SELECT user_id, CONCAT_WS(' ', `name`, `lname`) AS 'evaName' FROM mas_user) AS mas_user ON mas_user.`user_id` = round_point.`user_id` LEFT JOIN point_type ON point_type.`pointType_id` = round_point.`pointType_id`"

        dataset.Tables("round_point").Clear()
        condb.GetTable(sql, dataset.Tables("round_point"))
    End Sub

    Public Sub getclinic()
        Dim sql As String
        sql = ""
    End Sub
End Class
