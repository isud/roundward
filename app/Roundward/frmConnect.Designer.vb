﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConnect
    Inherits System.Windows.Forms.UserControl

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SCHMEDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.PASSDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.PORTDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.IPDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.USERDBTXT = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.txtPICPATH = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.SCHMEDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PASSDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PORTDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IPDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.USERDBTXT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPICPATH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.txtPICPATH)
        Me.LayoutControl1.Controls.Add(Me.SimpleButton1)
        Me.LayoutControl1.Controls.Add(Me.SCHMEDBTXT)
        Me.LayoutControl1.Controls.Add(Me.PASSDBTXT)
        Me.LayoutControl1.Controls.Add(Me.PORTDBTXT)
        Me.LayoutControl1.Controls.Add(Me.IPDBTXT)
        Me.LayoutControl1.Controls.Add(Me.USERDBTXT)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(739, 115, 423, 524)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(391, 235)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(197, 156)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(182, 22)
        Me.SimpleButton1.StyleController = Me.LayoutControl1
        Me.SimpleButton1.TabIndex = 9
        Me.SimpleButton1.Text = "Update"
        '
        'SCHMEDBTXT
        '
        Me.SCHMEDBTXT.Location = New System.Drawing.Point(72, 108)
        Me.SCHMEDBTXT.Name = "SCHMEDBTXT"
        Me.SCHMEDBTXT.Size = New System.Drawing.Size(307, 20)
        Me.SCHMEDBTXT.StyleController = Me.LayoutControl1
        Me.SCHMEDBTXT.TabIndex = 8
        '
        'PASSDBTXT
        '
        Me.PASSDBTXT.Location = New System.Drawing.Point(72, 84)
        Me.PASSDBTXT.Name = "PASSDBTXT"
        Me.PASSDBTXT.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PASSDBTXT.Size = New System.Drawing.Size(307, 20)
        Me.PASSDBTXT.StyleController = Me.LayoutControl1
        Me.PASSDBTXT.TabIndex = 7
        '
        'PORTDBTXT
        '
        Me.PORTDBTXT.Location = New System.Drawing.Point(72, 36)
        Me.PORTDBTXT.Name = "PORTDBTXT"
        Me.PORTDBTXT.Size = New System.Drawing.Size(307, 20)
        Me.PORTDBTXT.StyleController = Me.LayoutControl1
        Me.PORTDBTXT.TabIndex = 6
        '
        'IPDBTXT
        '
        Me.IPDBTXT.Location = New System.Drawing.Point(72, 12)
        Me.IPDBTXT.Name = "IPDBTXT"
        Me.IPDBTXT.Size = New System.Drawing.Size(307, 20)
        Me.IPDBTXT.StyleController = Me.LayoutControl1
        Me.IPDBTXT.TabIndex = 5
        '
        'USERDBTXT
        '
        Me.USERDBTXT.Location = New System.Drawing.Point(72, 60)
        Me.USERDBTXT.Name = "USERDBTXT"
        Me.USERDBTXT.Size = New System.Drawing.Size(307, 20)
        Me.USERDBTXT.StyleController = Me.LayoutControl1
        Me.USERDBTXT.TabIndex = 4
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem6, Me.EmptySpaceItem1, Me.LayoutControlItem7})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(391, 235)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.USERDBTXT
        Me.LayoutControlItem1.CustomizationFormText = "USER"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(371, 24)
        Me.LayoutControlItem1.Text = "USER"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.IPDBTXT
        Me.LayoutControlItem2.CustomizationFormText = "IP"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(371, 24)
        Me.LayoutControlItem2.Text = "IP"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.PORTDBTXT
        Me.LayoutControlItem3.CustomizationFormText = "PORT"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(371, 24)
        Me.LayoutControlItem3.Text = "PORT"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.PASSDBTXT
        Me.LayoutControlItem4.CustomizationFormText = "PASSWORD"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(371, 24)
        Me.LayoutControlItem4.Text = "PASSWORD"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.SCHMEDBTXT
        Me.LayoutControlItem5.CustomizationFormText = "SCHEMA"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(371, 24)
        Me.LayoutControlItem5.Text = "SCHEMA"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.SimpleButton1
        Me.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(185, 144)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(186, 71)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 144)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(185, 71)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'txtPICPATH
        '
        Me.txtPICPATH.Location = New System.Drawing.Point(72, 132)
        Me.txtPICPATH.Name = "txtPICPATH"
        Me.txtPICPATH.Size = New System.Drawing.Size(307, 20)
        Me.txtPICPATH.StyleController = Me.LayoutControl1
        Me.txtPICPATH.TabIndex = 10
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.txtPICPATH
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 120)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(371, 24)
        Me.LayoutControlItem7.Text = "PICPATH"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(57, 13)
        '
        'frmConnect
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmConnect"
        Me.Size = New System.Drawing.Size(391, 235)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.SCHMEDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PASSDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PORTDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IPDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.USERDBTXT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPICPATH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents PASSDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents PORTDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents IPDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents USERDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SCHMEDBTXT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents txtPICPATH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
End Class
