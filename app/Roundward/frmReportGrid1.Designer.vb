﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportGrid1
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim XyDiagram1 As DevExpress.XtraCharts.XYDiagram = New DevExpress.XtraCharts.XYDiagram()
        Dim StackedLineSeriesView1 As DevExpress.XtraCharts.StackedLineSeriesView = New DevExpress.XtraCharts.StackedLineSeriesView()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.Search = New DevExpress.XtraEditors.SimpleButton()
        Me.ChartControl1 = New DevExpress.XtraCharts.ChartControl()
        Me.PivotGridControl1 = New DevExpress.XtraPivotGrid.PivotGridControl()
        Me.DtsetRound1 = New Roundward.dtsetRound()
        Me.fieldnormal1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldabnormal1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldsickleave1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fielderrandleave1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldworkLoad1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldempAccident1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldinsufMedical1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldpatDocument1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldfalseProcess1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldenvironment1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldcomplain1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldpatAccident1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldnoKin1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldfinanceProblem1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldmissMaterial1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldother1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldclinic1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldroundshift1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fielddateround1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldDATESERV1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldclinicname1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.fieldroundshiftname1 = New DevExpress.XtraPivotGrid.PivotGridField()
        Me.dateend = New DevExpress.XtraEditors.DateEdit()
        Me.datestart = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(StackedLineSeriesView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DtsetRound1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateend.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dateend.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datestart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datestart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.Search)
        Me.LayoutControl1.Controls.Add(Me.ChartControl1)
        Me.LayoutControl1.Controls.Add(Me.dateend)
        Me.LayoutControl1.Controls.Add(Me.datestart)
        Me.LayoutControl1.Controls.Add(Me.PivotGridControl1)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(997, 622)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'Search
        '
        Me.Search.Location = New System.Drawing.Point(855, 12)
        Me.Search.Name = "Search"
        Me.Search.Size = New System.Drawing.Size(130, 22)
        Me.Search.StyleController = Me.LayoutControl1
        Me.Search.TabIndex = 8
        Me.Search.Text = "Process"
        '
        'ChartControl1
        '
        Me.ChartControl1.DataSource = Me.PivotGridControl1
        XyDiagram1.AxisX.DateTimeScaleOptions.AggregateFunction = DevExpress.XtraCharts.AggregateFunction.None
        XyDiagram1.AxisX.DateTimeScaleOptions.ScaleMode = DevExpress.XtraCharts.ScaleMode.Automatic
        XyDiagram1.AxisX.Title.Text = "DATESERV"
        XyDiagram1.AxisX.VisibleInPanesSerializable = "-1"
        XyDiagram1.AxisY.Title.Text = "เหตุการณ์ปกติ"
        XyDiagram1.AxisY.VisibleInPanesSerializable = "-1"
        Me.ChartControl1.Diagram = XyDiagram1
        Me.ChartControl1.Legend.MaxHorizontalPercentage = 30.0R
        Me.ChartControl1.Legend.Visibility = DevExpress.Utils.DefaultBoolean.[True]
        Me.ChartControl1.Location = New System.Drawing.Point(12, 328)
        Me.ChartControl1.Name = "ChartControl1"
        Me.ChartControl1.SeriesDataMember = "Series"
        Me.ChartControl1.SeriesSerializable = New DevExpress.XtraCharts.Series(-1) {}
        Me.ChartControl1.SeriesTemplate.ArgumentDataMember = "Arguments"
        Me.ChartControl1.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.Qualitative
        Me.ChartControl1.SeriesTemplate.LabelsVisibility = DevExpress.Utils.DefaultBoolean.[True]
        Me.ChartControl1.SeriesTemplate.LegendTextPattern = "{A}"
        Me.ChartControl1.SeriesTemplate.ValueDataMembersSerializable = "Values"
        StackedLineSeriesView1.LineMarkerOptions.Color = System.Drawing.Color.FromArgb(CType(CType(238, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(225, Byte), Integer))
        StackedLineSeriesView1.MarkerVisibility = DevExpress.Utils.DefaultBoolean.[True]
        Me.ChartControl1.SeriesTemplate.View = StackedLineSeriesView1
        Me.ChartControl1.Size = New System.Drawing.Size(973, 282)
        Me.ChartControl1.TabIndex = 7
        '
        'PivotGridControl1
        '
        Me.PivotGridControl1.DataMember = "round_point"
        Me.PivotGridControl1.DataSource = Me.DtsetRound1
        Me.PivotGridControl1.Fields.AddRange(New DevExpress.XtraPivotGrid.PivotGridField() {Me.fieldnormal1, Me.fieldabnormal1, Me.fieldsickleave1, Me.fielderrandleave1, Me.fieldworkLoad1, Me.fieldempAccident1, Me.fieldinsufMedical1, Me.fieldpatDocument1, Me.fieldfalseProcess1, Me.fieldenvironment1, Me.fieldcomplain1, Me.fieldpatAccident1, Me.fieldnoKin1, Me.fieldfinanceProblem1, Me.fieldmissMaterial1, Me.fieldother1, Me.fieldclinic1, Me.fieldroundshift1, Me.fielddateround1, Me.fieldDATESERV1, Me.fieldclinicname1, Me.fieldroundshiftname1})
        Me.PivotGridControl1.Location = New System.Drawing.Point(12, 38)
        Me.PivotGridControl1.Name = "PivotGridControl1"
        Me.PivotGridControl1.Size = New System.Drawing.Size(973, 286)
        Me.PivotGridControl1.TabIndex = 4
        '
        'DtsetRound1
        '
        Me.DtsetRound1.DataSetName = "dtsetRound"
        Me.DtsetRound1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'fieldnormal1
        '
        Me.fieldnormal1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldnormal1.AreaIndex = 0
        Me.fieldnormal1.Caption = "เหตุการณ์ปกติ"
        Me.fieldnormal1.FieldName = "normal"
        Me.fieldnormal1.Name = "fieldnormal1"
        '
        'fieldabnormal1
        '
        Me.fieldabnormal1.AreaIndex = 0
        Me.fieldabnormal1.Caption = "เหตุการณ์ไม่ปกติ"
        Me.fieldabnormal1.FieldName = "abnormal"
        Me.fieldabnormal1.Name = "fieldabnormal1"
        '
        'fieldsickleave1
        '
        Me.fieldsickleave1.AreaIndex = 1
        Me.fieldsickleave1.Caption = "ลาป่วย"
        Me.fieldsickleave1.FieldName = "sickleave"
        Me.fieldsickleave1.Name = "fieldsickleave1"
        '
        'fielderrandleave1
        '
        Me.fielderrandleave1.AreaIndex = 2
        Me.fielderrandleave1.Caption = "ลากิจ"
        Me.fielderrandleave1.FieldName = "errandleave"
        Me.fielderrandleave1.Name = "fielderrandleave1"
        '
        'fieldworkLoad1
        '
        Me.fieldworkLoad1.AreaIndex = 3
        Me.fieldworkLoad1.Caption = "งานโหลด"
        Me.fieldworkLoad1.FieldName = "workLoad"
        Me.fieldworkLoad1.Name = "fieldworkLoad1"
        '
        'fieldempAccident1
        '
        Me.fieldempAccident1.AreaIndex = 4
        Me.fieldempAccident1.Caption = "อุบัติเหตุพนักงาน"
        Me.fieldempAccident1.FieldName = "empAccident"
        Me.fieldempAccident1.Name = "fieldempAccident1"
        '
        'fieldinsufMedical1
        '
        Me.fieldinsufMedical1.AreaIndex = 5
        Me.fieldinsufMedical1.Caption = "เครื่องมือ/เวชภัณฑ์ไม่เพียงพอ"
        Me.fieldinsufMedical1.FieldName = "insufMedical"
        Me.fieldinsufMedical1.Name = "fieldinsufMedical1"
        '
        'fieldpatDocument1
        '
        Me.fieldpatDocument1.AreaIndex = 6
        Me.fieldpatDocument1.Caption = "เอกสารผู้ป่วย"
        Me.fieldpatDocument1.FieldName = "patDocument"
        Me.fieldpatDocument1.Name = "fieldpatDocument1"
        '
        'fieldfalseProcess1
        '
        Me.fieldfalseProcess1.AreaIndex = 7
        Me.fieldfalseProcess1.Caption = "ระบบงานผิดพลาด"
        Me.fieldfalseProcess1.FieldName = "falseProcess"
        Me.fieldfalseProcess1.Name = "fieldfalseProcess1"
        '
        'fieldenvironment1
        '
        Me.fieldenvironment1.AreaIndex = 8
        Me.fieldenvironment1.Caption = "สิ่งแวดล้อม แสง/สี/เสียง"
        Me.fieldenvironment1.FieldName = "environment"
        Me.fieldenvironment1.Name = "fieldenvironment1"
        '
        'fieldcomplain1
        '
        Me.fieldcomplain1.AreaIndex = 9
        Me.fieldcomplain1.Caption = "ลูกค้าร้องเรียน"
        Me.fieldcomplain1.FieldName = "complain"
        Me.fieldcomplain1.Name = "fieldcomplain1"
        '
        'fieldpatAccident1
        '
        Me.fieldpatAccident1.AreaIndex = 10
        Me.fieldpatAccident1.Caption = "อุบัติเหตุผู้ป่วย"
        Me.fieldpatAccident1.FieldName = "patAccident"
        Me.fieldpatAccident1.Name = "fieldpatAccident1"
        '
        'fieldnoKin1
        '
        Me.fieldnoKin1.AreaIndex = 11
        Me.fieldnoKin1.Caption = "ไม่มีญาติ"
        Me.fieldnoKin1.FieldName = "noKin"
        Me.fieldnoKin1.Name = "fieldnoKin1"
        '
        'fieldfinanceProblem1
        '
        Me.fieldfinanceProblem1.AreaIndex = 12
        Me.fieldfinanceProblem1.Caption = "พิจารณาปัญหาทางการเงิน"
        Me.fieldfinanceProblem1.FieldName = "financeProblem"
        Me.fieldfinanceProblem1.Name = "fieldfinanceProblem1"
        '
        'fieldmissMaterial1
        '
        Me.fieldmissMaterial1.AreaIndex = 13
        Me.fieldmissMaterial1.Caption = "ของสูญหาย"
        Me.fieldmissMaterial1.FieldName = "missMaterial"
        Me.fieldmissMaterial1.Name = "fieldmissMaterial1"
        '
        'fieldother1
        '
        Me.fieldother1.AreaIndex = 14
        Me.fieldother1.Caption = "อื่นๆ"
        Me.fieldother1.FieldName = "other"
        Me.fieldother1.Name = "fieldother1"
        '
        'fieldclinic1
        '
        Me.fieldclinic1.AreaIndex = 15
        Me.fieldclinic1.Caption = "idClinic"
        Me.fieldclinic1.FieldName = "clinic"
        Me.fieldclinic1.Name = "fieldclinic1"
        '
        'fieldroundshift1
        '
        Me.fieldroundshift1.AreaIndex = 16
        Me.fieldroundshift1.Caption = "idRoundShift"
        Me.fieldroundshift1.FieldName = "round_shift"
        Me.fieldroundshift1.Name = "fieldroundshift1"
        '
        'fielddateround1
        '
        Me.fielddateround1.AreaIndex = 17
        Me.fielddateround1.Caption = "วันที่ลงข้อมูล"
        Me.fielddateround1.FieldName = "date_round"
        Me.fielddateround1.Name = "fielddateround1"
        '
        'fieldDATESERV1
        '
        Me.fieldDATESERV1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldDATESERV1.AreaIndex = 0
        Me.fieldDATESERV1.FieldName = "DATESERV"
        Me.fieldDATESERV1.Name = "fieldDATESERV1"
        '
        'fieldclinicname1
        '
        Me.fieldclinicname1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldclinicname1.AreaIndex = 0
        Me.fieldclinicname1.Caption = "แผนก"
        Me.fieldclinicname1.FieldName = "clinic_name"
        Me.fieldclinicname1.Name = "fieldclinicname1"
        '
        'fieldroundshiftname1
        '
        Me.fieldroundshiftname1.AreaIndex = 18
        Me.fieldroundshiftname1.Caption = "เวร"
        Me.fieldroundshiftname1.FieldName = "round_shift_name"
        Me.fieldroundshiftname1.Name = "fieldroundshiftname1"
        '
        'dateend
        '
        Me.dateend.EditValue = Nothing
        Me.dateend.Location = New System.Drawing.Point(660, 12)
        Me.dateend.Name = "dateend"
        Me.dateend.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateend.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.dateend.Size = New System.Drawing.Size(191, 20)
        Me.dateend.StyleController = Me.LayoutControl1
        Me.dateend.TabIndex = 6
        '
        'datestart
        '
        Me.datestart.EditValue = Nothing
        Me.datestart.Location = New System.Drawing.Point(413, 12)
        Me.datestart.Name = "datestart"
        Me.datestart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.datestart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.datestart.Size = New System.Drawing.Size(209, 20)
        Me.datestart.StyleController = Me.LayoutControl1
        Me.datestart.TabIndex = 5
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.EmptySpaceItem1})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(997, 622)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.PivotGridControl1
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(977, 290)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.datestart
        Me.LayoutControlItem2.Location = New System.Drawing.Point(367, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(247, 26)
        Me.LayoutControlItem2.Text = "วันที่"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(31, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.dateend
        Me.LayoutControlItem3.Location = New System.Drawing.Point(614, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(229, 26)
        Me.LayoutControlItem3.Text = "ถึงวันที่"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(31, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.ChartControl1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 316)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(977, 286)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.Search
        Me.LayoutControlItem5.Location = New System.Drawing.Point(843, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(134, 26)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(367, 26)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'frmReportGrid1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmReportGrid1"
        Me.Size = New System.Drawing.Size(997, 622)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(XyDiagram1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(StackedLineSeriesView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ChartControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PivotGridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DtsetRound1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateend.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dateend.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datestart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datestart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ChartControl1 As DevExpress.XtraCharts.ChartControl
    Friend WithEvents dateend As DevExpress.XtraEditors.DateEdit
    Friend WithEvents datestart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents PivotGridControl1 As DevExpress.XtraPivotGrid.PivotGridControl
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Search As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents DtsetRound1 As dtsetRound
    Friend WithEvents fieldnormal1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldabnormal1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldsickleave1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fielderrandleave1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldworkLoad1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldempAccident1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldinsufMedical1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldpatDocument1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldfalseProcess1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldenvironment1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldcomplain1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldpatAccident1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldnoKin1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldfinanceProblem1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldmissMaterial1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldother1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldclinic1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldroundshift1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fielddateround1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldDATESERV1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldclinicname1 As DevExpress.XtraPivotGrid.PivotGridField
    Friend WithEvents fieldroundshiftname1 As DevExpress.XtraPivotGrid.PivotGridField
End Class
