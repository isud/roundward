﻿Imports System.Threading
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.XtraSplashScreen

Public Class Form1
    Dim frmreportGrid As frmReportGrid
    Dim frmreportGrid1 As frmReportGrid1
    Dim frmreportGrid2 As frmReportGrid2
    Dim frmreportGrid3 As frmReportGrid3
    Dim frmConnect As frmConnect

    Dim reportname As String
    Public Shared printSystem As New PrintingSystem

    Dim filename As String = "D:\\fProject\\test"

#Region "export"
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        SplashScreenManager.ShowForm(Me, GetType(SplashScreen1), True, True, False)

        For i As Integer = 1 To 400
            '       SplashScreenManager.Default.SetWaitFormDescription(i.ToString() & "%")
            'To process commands, override the SplashScreen.ProcessCommand method.
            Thread.Sleep(25)
        Next i
        ' Add any initialization after the InitializeComponent() call.
        SplashScreenManager.CloseForm()
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Shared Sub ExportData()

        ' Create objects and define event handlers.
        Dim composLink As CompositeLink = New CompositeLink(printSystem)
        'AddHandler composLink.CreateMarginalHeaderArea, AddressOf composLink_CreateMarginalHeaderArea
        Dim pcLinkPivot As New PrintableComponentLink
        Dim pcLinkChart1 As New PrintableComponentLink
        Dim pcLinkChart2 As New PrintableComponentLink

        Dim linkMainReport As New Link
        Dim linkPivotGrid As New Link

        If Form1.reportname = "frmreportGrid" Then
            pcLinkPivot.Component = Form1.frmreportGrid.PivotGridControl1
            pcLinkChart1.Component = Form1.frmreportGrid.ChartControl1
            pcLinkChart2.Component = Form1.frmreportGrid.ChartControl2
            composLink.Links.Add(pcLinkPivot)
            composLink.Links.Add(pcLinkChart1)
            composLink.Links.Add(pcLinkChart2)
            composLink.CreateDocument()
        ElseIf Form1.reportname = "frmreportGrid1" Then
            pcLinkPivot.Component = Form1.frmreportGrid1.PivotGridControl1
            pcLinkChart1.Component = Form1.frmreportGrid1.ChartControl1
            composLink.Links.Add(pcLinkPivot)
            composLink.Links.Add(pcLinkChart1)
            composLink.CreateDocument()

        ElseIf Form1.reportname = "frmreportGrid2" Then
            pcLinkPivot.Component = Form1.frmreportGrid2.gcPoint
            composLink.Links.Add(pcLinkPivot)
            composLink.CreateDocument()
        ElseIf Form1.reportname = "frmreportGrid3" Then
            pcLinkPivot.Component = Form1.frmreportGrid3.GridControl1
            composLink.Links.Add(pcLinkPivot)
            composLink.CreateDocument()
        Else

        End If
        ' Assign the controls to the printing links.

    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        Try
            FolderBrowserDialog1.Description = "Pick Folder to store PDF files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then

                ExportData()
                printSystem.ExportToPdf(FolderBrowserDialog1.SelectedPath & "\" & reportname & ".pdf")

                System.Diagnostics.Process.Start(FolderBrowserDialog1.SelectedPath & "\" & reportname & ".pdf")
            End If


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BarButtonItem1_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        Try
            FolderBrowserDialog1.Description = "Pick Folder to store Excel files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then

                ExportData()
                printSystem.ExportToXlsx(FolderBrowserDialog1.SelectedPath & "\" & reportname & ".xlsx")

                System.Diagnostics.Process.Start(FolderBrowserDialog1.SelectedPath & "\" & reportname & ".xlsx")
            End If


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub BarButtonItem2_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        Try
            FolderBrowserDialog1.Description = "Pick Folder to store HTML files"
            FolderBrowserDialog1.ShowNewFolderButton = True
            FolderBrowserDialog1.SelectedPath = "C:\"
            If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then

                ExportData()
                printSystem.ExportToHtml(FolderBrowserDialog1.SelectedPath & "\" & reportname & ".html")

                System.Diagnostics.Process.Start(FolderBrowserDialog1.SelectedPath & "\" & reportname & ".html")
            End If


        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
#End Region

   
#Region "print"
    Private Sub BarButtonItem4_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        ExportData()
        printSystem.Print()

    End Sub

    Private Sub BarButtonItem5_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        ExportData()
        printSystem.PrintDlg()

    End Sub
#End Region


#Region "link"

    Private Sub PUR0000_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles PUR0000.LinkClicked
        frmreportGrid = New frmReportGrid()
        reportname = "frmreportGrid"


        'AddHandler frmreportGrid.SimpleButton1.Click, AddressOf PanelGrid
        frmreportGrid.Dock = DockStyle.Fill
        PanelControl2.Controls.Clear()
        PanelControl2.Controls.Add(frmreportGrid)
    End Sub

    Private Sub NavBarItem2_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem2.LinkClicked
        frmreportGrid1 = New frmReportGrid1()
        reportname = "frmreportGrid1"


        'AddHandler frmreportGrid.SimpleButton1.Click, AddressOf PanelGrid
        frmreportGrid1.Dock = DockStyle.Fill
        PanelControl2.Controls.Clear()
        PanelControl2.Controls.Add(frmreportGrid1)
    End Sub

    Private Sub NavBarItem1_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem1.LinkClicked
        frmreportGrid2 = New frmReportGrid2()
        reportname = "frmreportGrid2"


        'AddHandler frmreportGrid.SimpleButton1.Click, AddressOf PanelGrid
        frmreportGrid2.Dock = DockStyle.Fill
        PanelControl2.Controls.Clear()
        PanelControl2.Controls.Add(frmreportGrid2)
    End Sub

    Private Sub nviComplain_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles nviComplain.LinkClicked
        frmReportGrid3 = New frmReportGrid3()
        reportname = "frmreportGrid3"


        'AddHandler frmreportGrid.SimpleButton1.Click, AddressOf PanelGrid
        frmReportGrid3.Dock = DockStyle.Fill
        PanelControl2.Controls.Clear()
        PanelControl2.Controls.Add(frmReportGrid3)
    End Sub

    Private Sub NavBarItem8_LinkClicked(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarItem8.LinkClicked
        frmConnect = New frmConnect()
        reportname = "nothinh"


        'AddHandler frmreportGrid.SimpleButton1.Click, AddressOf PanelGrid
        frmConnect.Dock = DockStyle.Fill
        PanelControl2.Controls.Clear()
        PanelControl2.Controls.Add(frmConnect)
    End Sub
#End Region

End Class
